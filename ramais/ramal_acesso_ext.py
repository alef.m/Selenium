# -*- coding: utf-8 -*-

import os
import sys
import logging
import time

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")


from output import console
from ramais_tools import RamaisTools
from driver_tools import DriverTools

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

""" Arquivo ramal_acesso_ext.py, este arquivo é utilizado para chamar todos os
	testes presentes na tela de Originação de Chamadas > Acesso Externo, ele é instanciado 
	pelo arquivo ramais.py ou então pode ser invocado via command line utilizando o comando
	python ramal_acesso_ext.py."""

class RamalAcessoExternoTest(object):
	""" Classe que contem todos os testes presentes na tela Originação de Chamadas > Acesso Externo.
		Parâmetros:
			driver: driver atual do navegador
			file_log: diretorio e nome do arquivo de log"""

	def __init__(self, driver, file_log):
		self.driver = driver
		self.file_log = file_log
		logging.basicConfig(filename=file_log,level=logging.ERROR,)


	def __salvar(self):
		""" Metodo responsavel por salvar as configurações do menu Originação de Chamadas > Acesso Externo"""
		estado = False
		try:
			self.driver.find_element_by_id("btnOriAceExtSalv").click()
			estado =  True
		except Exception:
			logging.exception('Erro na funcao salvar no arquivo ramal_acesso_ext.py')
		finally:
			console("Salvar - Originacao - Acesso Externo", estado).outTest()


	def __acessar_menu_acesso_externo(self):
		""" Metodo responsável por entrar no menu Originação de Chamadas > Acesso Externo"""
		estado = False

		try:
			self.driver.find_element_by_id("boxOriAceExt").click()
			estado = True
		except Exception:
			logging.exception('Erro na funcao __acessar_menu_acesso_externo no arquivo ramal_acesso_ext.py')
		finally:
			console("Acessar menu acesso externo", estado).outTest()


	def __definir_todos(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado de todos os checkbox de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtSelTodTur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtSelTodTur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtSelTodTur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtSelTodTur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()

			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_todos no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir todos acessos externos", estado).outTest()

	def __definir_local(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado local de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtLocTur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtLocTur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtLocTur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtLocTur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()
					
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_local no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir local - acessos externos", estado).outTest()

	def __definir_regional(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado reginal de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtRegTur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtRegTur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtRegTur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtRegTur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()
					
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_regional no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir regional - acessos externos", estado).outTest()

	def __definir_nacional(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado nacional de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtNacTur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtNacTur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtNacTur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtNacTur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()
					
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_nacional no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir nacional - acessos externos", estado).outTest()

	def __definir_internacional(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado internacional de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtIntTur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtIntTur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtIntTur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtIntTur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()
					
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_internacional no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir internacional - acessos externos", estado).outTest()

	def __definir_celular_local(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado celular local de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtCelLocTur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtCelLocTur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtCelLocTur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtCelLocTur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()
					
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_celular_local no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir celular local - acessos externos", estado).outTest()

	def __definir_celular_regional(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado celular regional de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtCelRegTur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtCelRegTur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtCelRegTur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtCelRegTur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()
					
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_celular_regional no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir celular regional - acessos externos", estado).outTest()

	def __definir_celular_nacional(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado celular nacional de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtCelNacTur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtCelNacTur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtCelNacTur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtCelNacTur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()
					
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_celular_nacional no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir celular nacional - acessos externos", estado).outTest()

	def __definir_celular_internacional(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado celular internacional de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtCelIntTur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtCelIntTur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtCelIntTur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtCelIntTur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()
					
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_celular_internacional no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir celular internacional - acessos externos", estado).outTest()

	def __definir_aux_1(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado auxiliar 1 de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtAux1Tur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtAux1Tur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtAux1Tur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtAux1Tur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()
					
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_aux_1 no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir auxiliar 1 - acessos externos", estado).outTest()

	def __definir_aux_2(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado auxiliar 2 de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtAux2Tur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtAux2Tur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtAux2Tur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtAux2Tur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()
					
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_aux_2 no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir auxiliar 2 - acessos externos", estado).outTest()

	def __definir_aux_3(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado auxiliar 3 de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtAux3Tur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtAux3Tur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtAux3Tur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtAux3Tur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()
					
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_aux_3 no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir auxiliar 3 - acessos externos", estado).outTest()

	def __definir_aux_4(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado auxiliar 4 de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtAux4Tur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtAux4Tur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtAux4Tur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtAux4Tur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()
					
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_aux_4 no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir auxiliar 4 - acessos externos", estado).outTest()

	def __definir_aux_5(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado auxiliar 5 de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtAux5Tur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtAux5Tur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtAux5Tur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtAux5Tur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()
					
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_aux_5 no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir auxiliar 5 - acessos externos", estado).outTest()

	def __definir_aux_6(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado auxiliar 6 de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtAux6Tur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtAux6Tur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtAux6Tur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtAux6Tur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()
					
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_aux_6 no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir auxiliar 6 - acessos externos", estado).outTest()

	def __definir_aux_7(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado auxiliar 7 de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtAux7Tur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtAux7Tur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtAux7Tur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtAux7Tur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()
					
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_aux_7 no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir auxiliar 7 - acessos externos", estado).outTest()

	def __definir_aux_8(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o estado auxiliar 8 de acesso externo do ramal que está sendo testado.
			Parâmetros: 
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceExtAux8Tur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtAux8Tur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceExtAux8Tur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceExtAux8Tur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()		

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()
					
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_aux_8 no arquivo ramal_acesso_ext.py')
		finally:
			console("Definir auxiliar 8 - acessos externos", estado).outTest()

	def make_test(self):
		""" Metodo responsável por chamar todos os testes pertencentes a tela Originação de Chamadas > Geral. """
		r_tools = RamaisTools(self.driver, self.file_log)

		# Acessa menu Ramal e o Ramal 201 - Este deve ser um ramal Digital
		r_tools.menu_ramal()
		time.sleep(4)
		r_tools.acesso_ramal(201)
		time.sleep(1)

		self.__acessar_menu_acesso_externo()
		time.sleep(1)

		self.__definir_local(True, True)
		self.__definir_regional(False, True)
		self.__definir_nacional(True, True)
		self.__definir_internacional(True, False)
		self.__definir_celular_local(False, False)
		self.__definir_celular_regional(True, True)
		self.__definir_celular_nacional(False, True)
		self.__definir_celular_internacional(False, True)

		self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

		self.__definir_aux_1(True, False)
		self.__definir_aux_2(False, True)
		self.__definir_aux_3(False, True)
		self.__definir_aux_4(True, True)
		self.__definir_aux_5(True, True)
		self.__definir_aux_6(False, False)
		self.__definir_aux_7(True, False)
		self.__definir_aux_8(True, True)

		time.sleep(1)
		self.__salvar()

		r_tools.menu_geral()

if __name__ == "__main__":
	path = os.getcwd()
	path = os.path.abspath('..')
	
	# Definicao do arquivo de log e inicializacao do driver
	file_log = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'
	driver = DriverTools(path).criar_driver_firefox()

	try:
		# Chamada do make_test
		RamalAcessoExternoTest(driver, file_log).make_test()
	finally:
		driver.close()