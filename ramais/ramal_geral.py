# -*- coding: utf-8 -*-

import os
import sys
import logging
import time

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")


from output import console
from ramais_tools import RamaisTools
from driver_tools import DriverTools

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

""" Arquivo ramal_geral.py, este arquivo é utilizado para chamar todos os
	testes presentes na tela Configurações Básicas > Geral, ele é instanciado
	pelo arquivo ramais.py ou então pode ser invocado via command line utilizando
	o comando python ramal_geral.py."""

class RamalGeralTest(object):
	""" Classe que contem todos os testes presentes na tela Configurações Básicas > Geral.
		Parâmetros:
			driver: driver atual do navegador
			numero: numero principal utilizado nos testes
			file_log: diretorio e nome do arquivo de log"""

	def __init__(self, driver, numero, file_log):
		self.driver = driver
		self.numero = numero
		self.file_log = file_log
		logging.basicConfig(filename=self.file_log,level=logging.ERROR,)


	def __salvar(self):
		""" Metodo responsável por salvar as configurações da tela Configurações Básicas > Geral. """

		estado = False
		try:
			# Salvar configuracao
			self.driver.find_element_by_id("btnConfBasGerSalv").click()

			estado = True
		except Exception:
			logging.exception('Erro na funcao salvar no arquivo ramal_geral.py')
			pass
		finally:
			console("Salvar - Ramal Geral", estado).outTest()

	def __acessa_menu_ramal_geral(self):
		""" Metodo respnsável por acessar o menu Configurações Básicas > Geral. """

		estado = False
		try:
			# Acesso ao menu Geral
			self.driver.find_element_by_id("boxConfBasGer").click()

			estado = True
		except Exception:
			logging.exception('Erro na funcao acessa_menu_ramal_geral no arquivo ramal_geral.py')
			pass			
		finally:
			console("Acessar menu - Ramal Geral", estado).outTest()

	def __alterar_numero(self):
		""" Metodo responsável por alterar o numero de um ramal.
			Return:
				novoNumero: Novo numero que foi atribuido ao ramal"""

		estado = False
		novoNumero = 100

		try:
			# Procura o campo de texto de numero
			txtNumero = self.driver.find_element_by_id("txtConfBasGerNum")

			# Caso o numero do ramal seja 100 ele troca para 200, caso for 200 ele troca para 100
			if (txtNumero.get_attribute('value') == "100"):
				novoNumero += 100

			# Limpa o campo
			txtNumero.clear()

			# Envia o novo numero
			txtNumero.send_keys(novoNumero)

			estado = True

		except Exception as error:
			logging.exception('Erro na funcao alterar_numero no arquivo ramal_geral.py')
			pass
		finally:
			console("Alterar numero do ramal", estado).outTest()

		return novoNumero


	def __alterar_apelido(self):
		""" Metodo responável por alterar o apelido de um ramal. """

		estado = False

		try:
			# Procura o campo de texto de apelido
			txtApelido = self.driver.find_element_by_id("txtConfBasGerApel")

			# Limpa o campo
			txtApelido.clear()

			# Envia o novo apelido
			txtApelido.send_keys(7)

			estado = True
		except Exception:
			logging.exception('Erro na funcao alterar_apelido no arquivo ramal_geral.py')
			pass
		finally:
			console("Alterar apelido do ramal", estado).outTest()

	def __alterar_nome(self):
		""" Metodo responsável por alterar o nome de um ramal. """

		estado = False
		try:
			# Procura o campo de texto de nome
			txtNome = self.driver.find_element_by_id("txtConfBasGerNomUsu")

			# Limpa o campo
			txtNome.clear()

			# Envia o novo nome
			txtNome.send_keys(600)

			estado = True
		except Exception:
			logging.exception('Erro na funcao alterar_nome no arquivo ramal_geral.py')
			pass
		finally:
			console("Alterar nome do ramal", estado).outTest()

	def __definir_operadora(self, operadora):
		""" Metodo responsável por definir se o ramal é operadora ou não.
			Parâmetros:
				operadora: True = Operadora | False = Não Operadora """

		estado = False	

		try:
			# Label do checkbox que sera clickado
			check = self.driver.find_element(By.XPATH, "//label[@for='chkConfBasGerAdm']")

			# Verificador se o checkbox esta habilitado ou nao
			verifCheck = self.driver.find_element_by_id('chkConfBasGerAdm').is_selected()

			# Definicao da checkbox de senha obrigatoria
			if operadora:

				if verifCheck:
					estado = True
				else:		
					check.click()
					estado = True
			else:
				if verifCheck:
					check.click()
					estado = True
				else:
					estado = True
		except Exception:
			logging.exception('Erro na funcao definir_operadora no arquivo ramal_geral.py')
			pass
		finally:			
			console("Definir como operadora", estado).outTest()

	def make_test(self):
		""" Metodo responsável por chamar todos os testes pertencentes a tela Configurações Básicas > Geral. """
		r_tools = RamaisTools(self.driver, self.file_log)

		# Acessa menu Ramal e o Ramal 200
		r_tools.menu_ramal()
		time.sleep(4)
		r_tools.acesso_ramal(200)

		# Acessa menu 
		self.__acessa_menu_ramal_geral()

		# Bloco de alteracao de numero apelido nome e define se o ramal e operadora
		numero = self.__alterar_numero()
		self.__alterar_apelido()
		self.__alterar_nome()
		self.__definir_operadora(True)
		self.__salvar()

		r_tools.menu_geral()

if __name__ == "__main__":
	path = os.getcwd()
	path = os.path.abspath('..')
	
	# Definicao do arquivo de log e inicializacao do driver
	file_log = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'
	driver = DriverTools(path).criar_driver_firefox()

	try:
		# Chamada do make_test
		RamalGeralTest(driver, 200, file_log).make_test()
	finally:
		driver.close()


