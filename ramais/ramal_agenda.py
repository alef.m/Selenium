# -*- coding: utf-8 -*-[]

import os
import sys
import logging
import time

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")

from driver_tools import DriverTools
from datetime import datetime
from selenium import webdriver
from output import console
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from ramais_tools import RamaisTools

"""Classe que contém todos os testes referentes à tela Agenda nas configurações de um ramal.
	Parâmetros:
		Habilitar = Se deve habilitar o checkbox (true) ou se deve desabilitar o checkbox (false)"""

class RamalAgendaTest(object):	
	def __init__(self, driver, file_log):
		self.driver = driver
		self.file_log = file_log
		logging.basicConfig(filename=file_log,level=logging.ERROR,)

	def __acessar_menu_agenda (self):
		"""Função que acessa o menu Agenda nas configurações de um ramal"""
		estado = False
		try:
			#Acesso ao menu Agenda
			self.driver.find_element_by_id("boxFuncAge").click()
			estado = True
			#Print na tela de saida sobre status de acesso ao menu Recebimento Geral dentro do ramal
		except Exception:
			logging.exception('Erro na funcao acessar_menu_agenda no arquivo ramais_agenda.py')
			pass		
		finally:
			console("Acessar menu Ramal - Agenda", estado).outTest()

	def __definir_habilitar(self, habilitar):
		"""Função específica para habilitar a checkbox  Habilitar em Ramal - Agenda """
		estado = False
		try:
			chk_habilitar = self.driver.find_element_by_id("chkFunAgeHab")
			if habilitar:
				
				#Verifica se o check box Habilitar está selecionado
				if not chk_habilitar.is_selected():
					self.driver.find_element(By.XPATH, "//label[@for='chkFunAgeHab']").click()

			else:
				if chk_habilitar.is_selected():
					self.driver.find_element(By.XPATH, "//label[@for='chkFunAgeHab']").click()
					
			estado = True

		except Exception:
			logging.exception('Erro na funcao __definir_habilitar no arquivo ramais_agenda.py')
			pass		
		finally:
			console("Habilitar Agenda", estado).outTest()

	

	def __definir_terminal(self, habilitar):
		"""Função específica para testar o checkbox Terminal no menu Ramal - Agenda
		Parâmetros:
			Habilitar = Se o checkbox deve ser habilitado (true) ou não (false)."""

		estado = False
		try:
			chk_terminal = self.driver.find_element_by_id("chkFunAgeTer")
			if habilitar:
				#Se o checkbox estiver clicável (Habilitar tem que estar habilitado)			
				if not chk_terminal.is_selected():
					self.driver.find_element(By.XPATH, "//label[@for='chkFunAgeTer']").click()			
			else:					
				if chk_terminal.is_selected():
					self.driver.find_element(By.XPATH, "//label[@for='chkFunAgeTer']").click()
			estado = True

		except Exception:
			logging.exception('Erro na funcao __definir_terminal no arquivo ramais_agenda.py')
			pass		
		finally:
			console("Habilidar Terminal", estado).outTest()

	def __definir_programador(self, habilitar):
		"""Função específica para testar checkbox Programador no menu Ramal - Agenda
			Parâmetros:
				Habilitar = Se o checkbox deve ser habilitado (true) ou não (false)"""
		estado = False
		try:
			chk_programador = self.driver.find_element_by_id("chkFunAgePro")
		
			if habilitar:
				#Se o checkbox estiver clicável (Habilitar tem que estar habilitado)			
				if not chk_programador.is_selected():
					self.driver.find_element(By.XPATH, "//label[@for='chkFunAgePro']").click()

			else:					
				if chk_programador.is_selected():
					self.driver.find_element(By.XPATH, "//label[@for='chkFunAgePro']").click()
			estado = True

		except Exception:
			logging.exception('Erro na funcao __definir_programador no arquivo ramais_agenda.py')
			pass		
		finally:
			console("Habilitar Programador", estado).outTest()	

	def __definir_rota_filial(self):
		"""Função Específica para definir o combobox Rota/filial no menu Ramal - Agenda """
		estado = False
		try:
			cbRota = self.driver.find_element_by_id("cbxFunAgeRot")
			if cbRota.is_enabled():
				select_cb_rota = Select(cbRota)
				select_cb_rota.select_by_visible_text('Rota automática')
				estado = True

		except Exception:
			logging.exception('Erro na função __definir_rota_filial no arquivo ramais_agenda.py')
			pass				
		finally:
			console("Programa Rota", estado).outTest()

	def __definir_destino(self):
		estado = False
		self.driver.find_element_by_id('txtFunAgeDest').clear()
		txt_rota = self.driver.find_element_by_id('txtFunAgeDest')
		try:

			if txt_rota.is_enabled():
				txt_rota.send_keys("841234")
				estado = True

		except Exception:
			logging.exception('Erro na funcao __definir_destino no arquivo ramais_agenda.py')
			pass		
		finally:
			console("Definir Destino", estado).outTest()	


	def __definir_privilegiado(self, habilitar):
			"""Função específica para habilitar a checkbox  Privilegiado em Ramal - Agenda """
			estado = False
			try:
				chk_privilegiado = self.driver.find_element_by_id("chkAgendaNumPrivGrid0")
				if habilitar:					
					#Verifica se o check box Habilitar está selecionado
					if not chk_privilegiado.is_selected():
						self.driver.find_element(By.XPATH, "//label[@for='chkAgendaNumPrivGrid0']").click()

				else:
					if chk_privilegiado.is_selected():
						self.driver.find_element(By.XPATH, "//label[@for='chkAgendaNumPrivGrid0']").click()

				estado = True

			except Exception:
				logging.exception('Erro na funcao __definir_privilegiado no arquivo ramais_agenda.py')
				pass		
			finally:
				console("Habilitar Privilegiado", estado).outTest()


	def __salvar(self):
		estado = False
		"""Função específica para salvar os parâmetros alterados no menu Ramal - Agenda"""
		try:
			# Salvar configuracao
			self.driver.find_element_by_id("btnFunAgeSalv").click()
			estado = True
		except Exception:
			logging.exception('Erro na funcao salvar no arquivo ramal_agenda.py')
			pass
		finally:
			console("Salvar - Agenda", estado).outTest()




	def make_test(self):
		r_tools = RamaisTools(self.driver, self.file_log)
			
		#Acessa menu ramal e o ramal 200z
		r_tools.menu_ramal()
		time.sleep(5)
		r_tools.acesso_ramal(200)

		time.sleep(2)
		self.__acessar_menu_agenda()
		time.sleep(1)

		self.__definir_habilitar(True)
		self.__definir_programador(True)
		self.__salvar()

		time.sleep(2)
		self.__acessar_menu_agenda()
		self.__definir_terminal(True)
		self.__definir_rota_filial()
		self.__definir_destino()
		self.__definir_privilegiado(True)
		self.__salvar()





if __name__ == "__main__":
	path = os.getcwd()
	path = os.path.abspath('..')
	
	# Definicao do arquivo de log e inicializacao do driver
	file_log = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'
	driver = DriverTools(path).criar_driver_firefox()

	try:
		# Chamada do make_test
		RamalAgendaTest(driver, file_log).make_test()
	finally:
		driver.close()








