# -*- coding: utf-8 -*-

import os
import sys
import logging
import time

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")

from output import console
from loadConfig import configs
from ramal_senha import RamalSenhaTest
from ramal_geral import RamalGeralTest
from ramais_tools import RamaisTools
from driver_tools import DriverTools
from ramal_identificacao import RamalIdentTest
from ramais_recebimento_geral import RecebimentoGeralTest
from ramal_pre_atendimento import RamalPreAtendimento
from ramal_fila import RamalFilaTest
from ramal_desvios import RamalDesviosTest
from ramal_callback import RamalCallbackTest
from ramal_linha_executiva import RamalLinhaExecutivaTest
from ramal_originacao_geral import RamalOriginacaoGeralTest
from ramal_acesso_ext import RamalAcessoExternoTest
from ramal_cadeado import RamalCadeadoTest
from ramal_hotline import RamalHotlineTest

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

""" Arquivo ramais.py, este arquivo é utilizado na declaração dos testes
	de ramais no arquivo unnit.py ou então pode ser envocado via command
	line utilizando python ramais.py. Em ambos os casos o resultado será
	a execução de todos os testes pertencentes a ramais. """

class RamaisTest(object):
	""" Classe responsavel por testes de ramais.
		Parâmetros:
			driver: driver atual do navegador
			senha: senha do ramal_senha
			file_log: diretorio e nome do arquivo de log"""

	def __init__(self, driver, senha, file_log):
		self.driver = driver
		self.senha = senha
		self.file_log = file_log

	def make_test(self, numero):
		""" Metodo responsavel por chamar todos os testes pertencentes a ramais. 
			Parâmetros:
				numero: Numero do ramal principal utilizado no teste."""
		
		r_geral = RamalGeralTest(self.driver, numero, self.file_log)
		r_senha = RamalSenhaTest(self.driver, self.senha, self.file_log)
		r_tools = RamaisTools(self.driver, self.file_log)
		r_ident = RamalIdentTest(self.driver, self.file_log)
		r_rgeral = RecebimentoGeralTest(self.driver, self.file_log)
		r_pre_atd = RamalPreAtendimento(self.driver, self.file_log)
		r_fila = RamalFilaTest(self.driver, self.file_log)
		r_desvios = RamalDesviosTest(self.driver, self.file_log)
		r_callback = RamalCallbackTest(self.driver, self.file_log)
		r_linha_exec = RamalLinhaExecutivaTest(self.driver, self.file_log)
		r_orig_geral = RamalOriginacaoGeralTest(self.driver, self.file_log)
		r_acesso_ext = RamalAcessoExternoTest(self.driver, self.file_log)
		r_cadeado = RamalCadeadoTest(self.driver, self.file_log)
		r_hotline = RamalHotlineTest(self.driver, self.file_log)

		# Testes do Menu Geral
		r_geral.make_test()

		# Testes do Menu Senha
		r_senha.make_test()

		# Testes do Menu Identificacao
		r_ident.make_test()

		# Testes do Menu Recebimento - Geral
		r_rgeral.make_test()

		# Testes do Menu Pre Atendimento
		r_pre_atd.make_test()

		# Testes do Menu Fila
		r_fila.make_test()

		# Testes do Menu Desvio
		r_desvios.make_test()

		# Testes do Menu Callback
		r_callback.make_test()

		# Testes do Menu Linha Executiva
		r_linha_exec.make_test()

		# Testes do Menu Originacao - Geral
		r_orig_geral.make_test()

		# Testes do Menu Originacao - Acesso Externo
		r_acesso_ext.make_test()

		# Testes do Menu Cadeado
		r_cadeado.make_test()

		# Testes do Menu Hotline
		r_hotline.make_test()



if __name__ == '__main__':
	path = os.getcwd()
	path = os.path.abspath('..')
	
	# Definicao do arquivo de log e inicializacao do driver e senha do ramal
	file_log = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'
	senhaRamal = configs().load_config(path, 'senhaRamal')
	driver = DriverTools(path).criar_driver_firefox()

	try:
		# Chamada do make_test
		RamaisTest(driver, senhaRamal, file_log).make_test(200)
	finally:
		driver.close()
	

