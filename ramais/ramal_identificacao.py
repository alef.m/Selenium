# -*- coding: utf-8 -*-

import os
import sys
import logging
import time

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")


from output import console
from ramais_tools import RamaisTools
from driver_tools import DriverTools

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

""" Arquivo ramal_identificacao.py, este arquivo é utilizado para chamar todos os
	testes presentes na tela Configurações Básicas > Identificação, ele é instanciado
	pelo arquivo ramais.py ou então pode ser invocado via command line utilizando
	o comando python ramal_identificacao.py."""

class RamalIdentTest(object):
	""" Classe que contem todos os testes presentes na tela Configurações Básicas > Identificação.
		Parâmetros:
			driver: driver atual do navegador
			file_log: diretorio e nome do arquivo de log"""

	def __init__(self, driver, file_log):
		self.driver = driver
		self.file_log = file_log
		logging.basicConfig(filename=file_log,level=logging.ERROR,)

	def __salvar(self):
		""" Metodo responsável por salvar as configurações da tela Configurações Básicas > Identificação. """

		estado = False

		try:
			# Salvar configuracao
			self.driver.find_element_by_id("btnConfBasIdenSalv").click()
			estado = True
		except Exception:
			logging.exception('Erro na funcao salvar no arquivo ramal_identificacao.py')
			pass
		finally:
			console("Salvar - Ramal Identificacao", estado).outTest()

	def __identificacao_analogico(self):
		""" Metodo responsável por realizar o teste de identificação em ramais analógicos. """

		estado = False

		try:
			chkIntern = self.driver.find_element_by_id("chkConfBasIdenInt")
			chkExtern = self.driver.find_element_by_id("chkConfBasIdenExt")
			cbSinalizacao = self.driver.find_element_by_id("chkConfBasIdenSin")
			cbPausa = self.driver.find_element_by_id("chkConfBasIdenPau")

			# Identificacao do ramal digital deve ser desabilitado pois o protocolo digital ja realiza esta identificacao
			if(chkIntern.is_enabled() or chkExtern.is_enabled() or cbSinalizacao.is_enabled() or cbPausa.is_enabled()):

				checkIntern = self.driver.find_element(By.XPATH, "//label[@for='chkConfBasIdenInt']")
				checkExtern = self.driver.find_element(By.XPATH, "//label[@for='chkConfBasIdenExt']")

				checkIntern.click()
				checkExtern.click()

				select_cb_sinalizacao = Select(cbSinalizacao)
				select_cb_sinalizacao.select_by_visible_text('FSK Bell')

				select_cb_sinalizacao = Select(cbPausa)
				select_cb_sinalizacao.select_by_visible_text('120')

				self.driver.find_element(By.XPATH, "//label[@for='chkConfBasIdenOriE1']").click()
				self.driver.find_element(By.XPATH, "//label[@for='chkConfBasIdenIdeInt']").click()
				self.driver.find_element(By.XPATH, "//label[@for='chkConfBasIdenIdeExt']").click()


				estado = True
		except Exception:
			logging.exception('Erro na funcao identificacao_analogico no arquivo ramal_identificacao.py')
			pass				
		finally:
			console("Ramal Identificacao - Ramal Analogico", estado).outTest()

	def __identificacao_digital(self):
		""" Metodo responsável por realizar o teste de identificação em ramais digitais. """

		estado = True

		try:
			chkIntern = self.driver.find_element_by_id("chkConfBasIdenInt")
			chkExtern = self.driver.find_element_by_id("chkConfBasIdenExt")
			cbSinalizacao = self.driver.find_element_by_id("chkConfBasIdenSin")
			cbPausa = self.driver.find_element_by_id("chkConfBasIdenPau")

			# Identificacao do ramal digital deve ser desabilitado pois o protocolo digital ja realiza esta identificacao
			if(chkIntern.is_enabled() or chkExtern.is_enabled() or cbSinalizacao.is_enabled() or cbPausa.is_enabled()):
				estado = False
			
		except Exception:
			logging.exception('Erro na funcao identificacao_digital no arquivo ramal_identificacao.py')
			pass

		finally:
			console("Ramal Identificacao - Ramal Digital", estado).outTest()

	def __acessa_menu_ramal_identificacao(self):
		""" Metodo respnsável por acessar o menu Configurações Básicas > Identificação. """

		estado = False
		try:
			# Acesso ao menu Geral
			self.driver.find_element_by_id("boxConfBasIdent").click()

			estado = True
		except Exception:
			logging.exception('Erro na funcao acessa_menu_ramal_identificacao no arquivo ramal_identificacao.py')
			pass			
		finally:
			console("Acessar menu - Ramal Identificacao", estado).outTest()

	def __cancelar(self):
		""" Metodo responsável por cancelar as configurações da tela Configurações Básicas > Identificação. """

		try:
			# Salvar configuracao
			self.driver.find_element_by_id("btnConfBasIdenCanc").click()
		except Exception:
			logging.exception('Erro na funcao cancelar no arquivo ramal_identificacao.py')
			pass

	def make_test(self):
		""" Metodo responsável por chamar todos os testes pertencentes a tela Configurações Básicas > Identificação. """

		r_tools = RamaisTools(self.driver, self.file_log)

		# Acessa menu Ramal e o Ramal 201 - Este deve ser um ramal Digital
		r_tools.menu_ramal()
		time.sleep(4)
		r_tools.acesso_ramal(201)
		self.__acessa_menu_ramal_identificacao()
		self.__identificacao_digital()
		self.__cancelar()

		# Volta ao menu Geral
		r_tools.menu_geral()

		# Acessa menu Ramal e o Ramal 204 - Este deve ser um ramal Analogico
		r_tools.menu_ramal()
		time.sleep(4)
		r_tools.acesso_ramal(204)
		self.__acessa_menu_ramal_identificacao()
		self.__identificacao_analogico()
		self.__salvar()

		r_tools.menu_geral()

if __name__ == "__main__":
	path = os.getcwd()
	path = os.path.abspath('..')
	
	# Definicao do arquivo de log e inicializacao do driver
	file_log = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'
	driver = DriverTools(path).criar_driver_firefox()

	try:
		# Chamada do make_test
		RamalIdentTest(driver, file_log).make_test()
	finally:
		driver.close()