# -*- coding: utf-8 -*-

import os
import sys
import logging
import time

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")


from output import console
from ramais_tools import RamaisTools
from driver_tools import DriverTools

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

""" Arquivo ramal_conferencia.py, este arquivo é utilizado para chamar todos os
	testes presentes na tela de Funcionalidades > Conferencia, ele é instanciado 
	pelo arquivo ramais.py ou então pode ser invocado via command line utilizando o comando
	python ramal_conferencia.py."""

class RamalConferenciaTest(object):
	""" Classe que contem todos os testes presentes na tela Originação de Funcionalidades > Conferencia.
		Parâmetros:
			driver: driver atual do navegador
			file_log: diretorio e nome do arquivo de log"""

	def __init__(self, driver, file_log):
		self.driver = driver
		self.file_log = file_log
		logging.basicConfig(filename=file_log,level=logging.ERROR,)

	def __salvar(self):
		""" Metodo responsavel por salvar as configurações do menu Funcionalidades > Conferencia"""
		estado = False
		try:
			self.driver.find_element_by_id("btnFunConSalv").click()
			estado =  True
		except Exception:
			logging.exception('Erro na funcao salvar no arquivo ramal_conferencia.py')
		finally:
			console("Salvar - Conferencia", estado).outTest()


	def __acessar_menu_conferencia(self):
		""" Metodo responsável por entrar no menu Funcionalidades > Conferencia"""
		estado = False

		try:
			self.driver.find_element_by_id("boxFuncConf").click()
			estado = True
		except Exception:
			logging.exception('Erro na funcao __acessar_menu_conferencia no arquivo ramal_conferencia.py')
		finally:
			console("Acessar menu conferencia", estado).outTest()


	def __definir_conferencia(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por habilitar ou desabilitar conferencia no ramal que está sendo testado
			Parâmetros:
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkFunConHabTur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkFunConHabTur1']")

			chk_not = self.driver.find_element_by_id("chkFunConHabTur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkFunConHabTur2']")

			if habilitar_diu:
				if not chk_diu.is_enabled():
					lbl_diu.click()
			else:
				if chk_diu.is_enabled():
					lbl_diu.click()

			if habilitar_not:
				if not chk_not.is_enabled():
					lbl_not.click()
			else:
				if chk_not.is_enabled():
					lbl_not.click()
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_conferencia no arquivo ramal_conferencia.py')
		finally:
			console("Definir conferencia", estado).outTest()


	def __definir_conferencia_agendada(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por habilitar ou desabilitar conferencia agendada no ramal que está sendo testado
			Parâmetros:
				habilitar_diu: True = Habilitar / False = Desabilitar
				habilitar_not: True = Habilitar / False = Desabilitar"""
		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkFunConHabAgeTur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkFunConHabAgeTur1']")

			chk_not = self.driver.find_element_by_id("chkFunConHabAgeTur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkFunConHabAgeTur2']")

			if habilitar_diu:
				if not chk_diu.is_enabled():
					lbl_diu.click()
			else:
				if chk_diu.is_enabled():
					lbl_diu.click()

			if habilitar_not:
				if not chk_not.is_enabled():
					lbl_not.click()
			else:
				if chk_not.is_enabled():
					lbl_not.click()
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_conferencia_agendada no arquivo ramal_conferencia.py')
		finally:
			console("Definir conferencia agendada", estado).outTest()


	def make_test(self):
		""" Metodo responsável por chamar todos os testes pertencentes a tela Funcionalidades > Conferencia. """
		r_tools = RamaisTools(self.driver, self.file_log)

		# Acessa menu Ramal e o Ramal 201 - Este deve ser um ramal Digital
		r_tools.menu_ramal()
		time.sleep(4)
		r_tools.acesso_ramal(202)
		time.sleep(1)

		self.__acessar_menu_conferencia()
		time.sleep(1)

		self.__definir_conferencia(True, False)
		self.__definir_conferencia_agendada(False, True)
		self.__salvar()

		time.sleep(1)
		r_tools.menu_geral()



if __name__ == "__main__":
	path = os.getcwd()
	path = os.path.abspath('..')
	
	# Definicao do arquivo de log e inicializacao do driver
	file_log = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'
	driver = DriverTools(path).criar_driver_firefox()

	try:
		# Chamada do make_test
		RamalConferenciaTest(driver, file_log).make_test()
	finally:
		driver.close()