# -*- coding: utf-8 -*-

import os
import sys
import logging
import time

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")


from output import console
from ramais_tools import RamaisTools
from driver_tools import DriverTools

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

from selenium.webdriver.common.keys import Keys

""" Arquivo ramal_originacao_geral.py, este arquivo é utilizado para chamar todos os
	testes presentes na tela de Originação de Chamadas > Geral, ele é instanciado 
	pelo arquivo ramais.py ou então pode ser invocado via command line utilizando o comando
	python ramal_originacao_geral.py."""

class RamalOriginacaoGeralTest(object):
	""" Classe que contem todos os testes presentes na tela Originação de Chamadas > Geral.
		Parâmetros:
			driver: driver atual do navegador
			file_log: diretorio e nome do arquivo de log"""

	def __init__(self, driver, file_log):
		self.driver = driver
		self.file_log = file_log
		logging.basicConfig(filename=file_log,level=logging.ERROR,)

	def __salvar(self):
		""" Metodo responsavel por salvar as configurações do menu Originação de Chamadas > Geral"""
		estado = False
		try:
			self.driver.find_element_by_id("btnOriGerSalv").click()
			estado =  True
		except Exception:
			logging.exception('Erro na funcao salvar no arquivo ramal_originacao_geral.py')
		finally:
			console("Salvar - Originacao - Geral", estado).outTest()


	def __acessar_menu_originacao_geral(self):
		""" Metodo responsável por entrar no menu Originação de Chamadas > Geralv"""
		estado = False

		try:
			self.driver.find_element_by_id("boxOriGer").click()
			estado = True
		except Exception:
			logging.exception('Erro na funcao __acessar_menu_originacao_geral no arquivo ramal_originacao_geral.py')
		finally:
			console("Acessar menu Originacao Geral", estado).outTest()


	def __definir_tom_de_discar(self, habilitar):
		""" Metodo responsável por definir o tom de discagem no ramal que está sendo testado.
			Parâmetros:
				habilitar: True = Ativado / False = Desativado"""
		estado = False

		try:
			chk_tom = self.driver.find_element_by_id("chkOriGerTom")
			lbl_tom = self.driver.find_element(By.XPATH, "//label[@for='chkOriGerTom']")

			if habilitar:
				if not chk_tom.is_selected():
					lbl_tom.click()

			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_tom_de_discar no arquivo ramal_originacao_geral.py')
		finally:
			console("Definir tom de discagem", estado).outTest()


	def __definir_bloqueio_bilhetagem(self, habilitar):
		""" Metodo responsável por definir o bloqueio de bilhetagem no ramal que está sendo testado.
			Parâmetros:
				habilitar: True = Ativado / False = Desativado"""
		estado = False

		try:
			chk_bloq_bil = self.driver.find_element_by_id("chkOriBloqBil")
			lbl_bloq_bil = self.driver.find_element(By.XPATH, "//label[@for='chkOriBloqBil']")

			if habilitar:
				if not chk_bloq_bil.is_selected():
					lbl_bloq_bil.click()

			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_bloqueio_bilhetagem no arquivo ramal_originacao_geral.py')
		finally:
			console("Definir bloqueio de bilhetagem", estado).outTest()

	def __definir_duracao_max(self):
		""" Metodo responsavel por definir a duração máxima de Originação do ramal que está sendo testado."""
		estado = False

		try:
			cb_duracao = Select(self.driver.find_element_by_id("cbxOriDurMax"))
			cb_duracao.select_by_visible_text("40")

			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_duracao_max no arquivo ramal_originacao_geral.py')
		finally:
			console("Definir duracao maxima", estado).outTest()

	def __definir_acesso_ramal_ext(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir o acesso ramal externo no turno diurno e noturno.
			Parâmetros:
				habilitar_diu: True = Ativado / False = Desativado
				habilitar_not: True = Ativado / False = Desativado"""

		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriAceRamExtTur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceRamExtTur1']")

			chk_not = self.driver.find_element_by_id("chkOriAceRamExtTur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriAceRamExtTur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()

			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_acesso_ramal_ext no arquivo ramal_originacao_geral.py')
		finally:
			console("Definir acesso ramal externo", estado).outTest()


	def __definir_interna_condicionada(self, habilitar_diu, habilitar_not):
		""" Metodo responsável por definir interna condicionada no ramal ques está sendo testado.
			Parâmetros:
				habilitar_diu: True = Ativado / False = Desativado
				habilitar_not: True = Ativado / False = Desativado"""

		estado = False
		try:
			chk_diu = self.driver.find_element_by_id("chkOriIntConTur1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkOriIntConTur1']")

			chk_not = self.driver.find_element_by_id("chkOriIntConTur2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkOriIntConTur2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()
			else:
				if chk_diu.is_selected():
					lbl_diu.click()

			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()
			else:
				if chk_not.is_selected():
					lbl_not.click()

			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_interna_condicionada no arquivo ramal_originacao_geral.py')
		finally:
			console("Definir chamada interna condicionada", estado).outTest()


	def make_test(self):
		""" Metodo responsável por chamar todos os testes pertencentes a tela Originação de Chamadas > Geral. """

		r_tools = RamaisTools(self.driver, self.file_log)

		# Acessa menu Ramal e o Ramal 201 - Este deve ser um ramal Digital
		r_tools.menu_ramal()
		time.sleep(4)
		r_tools.acesso_ramal(201)
		time.sleep(1)
		self.__acessar_menu_originacao_geral()
		time.sleep(1)
		self.__definir_tom_de_discar(True)
		self.__definir_bloqueio_bilhetagem(True)
		self.__definir_duracao_max()
		self.__definir_acesso_ramal_ext(True, False)
		self.__definir_interna_condicionada(False, True)
		time.sleep(1)
		self.__salvar()
		time.sleep(1)

		r_tools.menu_geral()



if __name__ == "__main__":
	path = os.getcwd()
	path = os.path.abspath('..')
	
	# Definicao do arquivo de log e inicializacao do driver
	file_log = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'
	driver = DriverTools(path).criar_driver_firefox()

	try:
		# Chamada do make_test
		RamalOriginacaoGeralTest(driver, file_log).make_test()
	finally:
		driver.close()