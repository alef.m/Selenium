# -*- coding: utf-8 -*-

import os
import sys
import logging
import time

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")


from output import console
from ramais_tools import RamaisTools
from driver_tools import DriverTools
from loadConfig import configs

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

""" Arquivo ramal_senha.py, este arquivo é utilizado para chamar todos os
	testes presentes na tela Configurações Básicas > Senha, ele é instanciado
	pelo arquivo ramais.py ou então pode ser invocado via command line utilizando
	o comando python ramal_geral.py."""

class RamalSenhaTest(object):
	""" Classe que contem todos os testes presentes na tela Configurações Básicas > Senha.
		Parâmetros:
			driver: driver atual do navegador
			senha: senha que será utilizada no ramal
			file_log: diretorio e nome do arquivo de log"""

	def __init__(self, driver, senha, file_log):
		self.driver = driver
		self.senha = senha
		self.file_log = file_log
		logging.basicConfig(filename=file_log,level=logging.ERROR,)


	def __acessa_menu_ramal_senha(self):
		""" Metodo responsável por acessar o menu Configurações Básicas > Senha"""
		estado = False

		try:
			# Acesso ao menu Senha
			self.driver.find_element_by_id("boxConfBasSen").click()
			estado = True
		except Exception:
			logging.exception('Erro na funcao acessa_menu_ramal_senha no arquivo ramal_senha.py')
			pass
		finally:
			console("Acessa Menu - Ramal Senha", estado).outTest()

	def __salvar(self):
		""" Metodo responsável por salvar as configurações da tela Configurações Básicas > Senha. """

		estado = False

		try:
			# Salvar configuracao
			self.driver.find_element_by_id("btnConfBasSenSalv").click()
			estado = True
		except Exception:
			logging.exception('Erro na funcao salvar no arquivo ramal_senha.py')
			pass
		finally:
			console("Salvar - Ramal Senha", estado).outTest()

	def __programar_senha(self, programar):
		""" Metodo responsável por programar a senha no ramal;
			Parâmetros:
				programar: True = Obrigatorio senha para programar / False = Nao é obrigatorio senha para programar"""
		estado = False

		try:

			# Limpa o campo de senha
			self.driver.find_element_by_id("txtConfBasSenSen").clear()

			# Procura o texto e insere a senha do ramal
			self.driver.find_element_by_id("txtConfBasSenSen").send_keys(self.senha)

			# Label do checkbox que sera clickado
			check = self.driver.find_element(By.XPATH, "//label[@for='chkConfBasSenObrProg']")

			# Verificador se o checkbox esta habilitado ou nao
			verifCheck = self.driver.find_element_by_id('chkConfBasSenObrProg').is_selected()

			# Definicao da checkbox de senha obrigatoria
			if programar:

				if verifCheck:
					estado = True
				else:		
					check.click()
					estado = True
			else:
				if verifCheck:
					check.click()
					estado = True
				else:
					estado = True


		except Exception:
			logging.exception('Erro na funcao programar_senha no arquivo ramal_senha.py')
			pass
		finally:
			console("Programa senha", estado).outTest()

	def make_test(self):
		""" Metodo responsável por chamar todos os testes pertencentes a tela Configurações Básicas > Senha. """

		r_tools = RamaisTools(self.driver, self.file_log)

		# Acessa menu Ramal e o Ramal 200
		r_tools.menu_ramal()
		time.sleep(4)
		r_tools.acesso_ramal(200)

		self.__acessa_menu_ramal_senha()
		self.__programar_senha(True)
		self.__salvar()

		r_tools.menu_geral()

if __name__ == "__main__":
	path = os.getcwd()
	path = os.path.abspath('..')
	
	# Definicao do arquivo de log e inicializacao do driver
	file_log = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'
	driver = DriverTools(path).criar_driver_firefox()
	senha = configs().load_config(path, 'senhaRamal')

	try:
		# Chamada do make_test
		RamalSenhaTest(driver, senha, file_log).make_test()
	finally:
		driver.close()

