# -*- coding: utf-8 -*-

import os
import sys
import logging
import time

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")


from output import console
from ramais_tools import RamaisTools
from driver_tools import DriverTools

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

""" Arquivo ramal_pre_atendimento.py, este arquivo é utilizado para chamar todos os
	testes presentes na tela de Recebimento Geral > Pré-Atendimento, ele é instanciado 
	pelo arquivo ramais.py ou então pode ser invocado via command line utilizando o comando
	python ramal_pre_atendimento.py."""

class RamalPreAtendimento(object):
	""" Classe que contem todos os testes presentes na tela Recebimento Geral > Pré-Atendimento.
		Parâmetros:
			driver: driver atual do navegador
			file_log: diretorio e nome do arquivo de log"""
	
	def __init__(self, driver, file_log):
		self.driver = driver
		self.file_log = file_log
		logging.basicConfig(filename=file_log,level=logging.ERROR,)

	def __salvar(self):
		""" Metodo responsavel por salvar as configurações do menu Pré-Atendimento"""
		estado = False
		try:
			# Salvar configuracao
			self.driver.find_element_by_id("btnRecPreAtdSalv").click()

			estado = True
		except Exception:
			logging.exception('Erro na funcao salvar no arquivo ramais_pre_atendimento.py')
			pass			
		finally:
			console("Salvar - Ramal Pre Atendimento", estado).outTest()

	def __acessa_menu_pre_atendimento(self):
		""" Metodo responsável por acessar o menu de Pré-Atendimento. """
		estado = False
		try:
			# Acesso ao menu pre atendimento
			self.driver.find_element_by_id("boxRecPreAtend").click()

			estado = True
		except Exception:
			logging.exception('Erro na funcao acessa_menu_pre_atendimento no arquivo ramais_pre_atendimento.py')			
			pass			
		finally:
			console("Acessar menu - Ramal Pre Atendimento", estado).outTest()

	def __definir_atendedor_ramal(self, atendedor, tipo):
		""" Metodo responsável por definir o atendedor do ramal
			Parâmetros:
				atendedor: Ramal que sera o atendedor das chamadas
				tipo: True = Sempre / False = Chamada não atendida"""
		estado = False

		try:
			cbTransf = self.driver.find_element_by_id("cbxRecPreAtdAutRamalTra")
			cbAtendedor = self.driver.find_element_by_id("cbxRecPreAtdAutRamalAtd")
			if not cbTransf.is_selected():

				select_cbAtendedor = Select(cbAtendedor)
				select_cbAtendedor.select_by_visible_text(str(atendedor))

				if cbTransf.is_enabled():
					select_cbTransf = Select(cbTransf)
					
					if tipo:
						select_cbTransf.select_by_visible_text("Sempre")
					else:
						select_cbTransf.select_by_visible_text("Chamada não atendida")

					estado = True
		except Exception:
			logging.exception('Erro na funcao definir_atendedor_ramal no arquivo ramais_pre_atendimento.py')
			pass
		finally:
			console("Definir atendedor de ramal pre atendimento", estado).outTest()


	def __definir_atendedor_fax(self, atendedor, tipo):
		""" Metodo responsável por definir o atendedor de fax.
			Parâmetros:
				atendedor = Ramal que será o atendedor das chamadas de fax.
				tipo: True = Sempre / False = Chamada não atendida"""
		estado = False

		try:
			cbTransf = self.driver.find_element_by_id("cbxRecPreAtdAutFaxTra")
			cbAtendedor = self.driver.find_element_by_id("cbxRecPreAtdAutFaxAtd")
			if not cbTransf.is_selected():

				select_cbAtendedor = Select(cbAtendedor)
				select_cbAtendedor.select_by_visible_text(str(atendedor))

				if cbTransf.is_enabled():
					select_cbTransf = Select(cbTransf)
					
					if tipo:
						select_cbTransf.select_by_visible_text("Sempre")
					else:
						select_cbTransf.select_by_visible_text("Chamada não atendida")

					estado = True
		except Exception:
			logging.exception('Erro na funcao definir_atendedor_fax no arquivo ramais_pre_atendimento.py')
			pass
		finally:
			console("Definir atendedor de fax pre atendimento", estado).outTest()


	def make_test(self):
		""" Metodo responsável por chamar todos os testes pertencentes a tela Recebimento Geral > Pré-Atendimento. """

		r_tools = RamaisTools(self.driver, self.file_log)

		# Acessa menu Ramal e o Ramal 204
		r_tools.menu_ramal()
		time.sleep(4)
		r_tools.acesso_ramal(204)

		self.__acessa_menu_pre_atendimento()
		time.sleep(2)
		self.__definir_atendedor_ramal(206, True)
		self.__salvar()

		time.sleep(2)
		self.__acessa_menu_pre_atendimento()
		self.__definir_atendedor_fax(205, False)
		self.__salvar()

		r_tools.menu_geral()

if __name__ == "__main__":
	path = os.getcwd()
	path = os.path.abspath('..')
	
	# Definicao do arquivo de log e inicializacao do driver
	file_log = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'
	driver = DriverTools(path).criar_driver_firefox()

	try:
		# Chamada do make_test
		RamalPreAtendimento(driver, file_log).make_test()
	finally:
		driver.close()