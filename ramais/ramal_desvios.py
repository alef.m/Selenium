# -*- coding: utf-8 -*-

import os
import sys
import logging
import time

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")


from output import console
from ramais_tools import RamaisTools
from driver_tools import DriverTools
from loadConfig import configs

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

""" Arquivo ramal_desvios.py, este arquivo é utilizado para chamar todos os
	testes presentes na tela de Recebimento Geral > Desvios, ele é instanciado 
	pelo arquivo ramais.py ou então pode ser invocado via command line utilizando o comando
	python ramal_desvios.py."""

class RamalDesviosTest(object):
	""" Classe que contem todos os testes presentes na tela Recebimento Geral > Desvios.
		Parâmetros:
			driver: driver atual do navegador
			file_log: diretorio e nome do arquivo de log"""

	def __init__(self, driver, file_log):
		self.driver = driver
		self.file_log = file_log
		logging.basicConfig(filename=file_log,level=logging.ERROR,)


	def __salvar(self):
		""" Metodo responsavel por salvar as configurações do menu Recebimento de Chamadas > Desvios"""
		estado = False
		try:
			self.driver.find_element_by_id("btnRecDesSalv").click()
			estado =  True
		except Exception:
			logging.exception('Erro na funcao salvar no arquivo ramal_desvios.py')
		finally:
			console("Salvar - Desvios", estado).outTest()


	def __acessar_menu_desvios(self):
		""" Metodo responsável por entrar no menu Recebimento de Chamadas > Desvios"""
		estado = False

		try:
			self.driver.find_element_by_id("boxRecDesv").click()
			estado = True
		except Exception:
			logging.exception('Erro na funcao acessar_menu_desvios no arquivo ramal_desvios.py')
		finally:
			console("Acessar menu Desvios", estado).outTest()


	def __programar_tipo_desvios(self, externo_interno, externo_externo):
		""" Metodo responsável por programar os tipos de desvios que são permitidos no ramal.
			Parâmetros:
				externo_interno: True = Habilita desvio externo de chamadas internas / False = Desabilita
				externo_externo: True = Habilita desvio esterno de chamadas externas / False = Desabilita"""

		estado = False
		try:
			chk_ext_int = self.driver.find_element_by_id("chkRecDesPerDesExtInt")
			lbl_ext_int = self.driver.find_element(By.XPATH, "//label[@for='chkRecDesPerDesExtInt']")

			chk_ext_ext = self.driver.find_element_by_id("chkRecDesPerDesExtExt")
			lbl_ext_ext = self.driver.find_element(By.XPATH, "//label[@for='chkRecDesPerDesExtExt']")

			if externo_interno:
				if not chk_ext_int.is_selected():
					lbl_ext_int.click()
			else:
				if chk_ext_int.is_selected():
					lbl_ext_int.click()

			if externo_externo:
				if not chk_ext_ext.is_selected():
					lbl_ext_ext.click()
			else:
				if chk_ext_ext.is_selected():
					lbl_ext_ext.click()
		
			self.__salvar()
			estado = True
		except Exception:
			logging.exception('Erro na funcao programar_tipo_desvios no arquivo ramal_desvios.py')
		finally:
			console("Programar tipo de Desvios", estado).outTest()


	def __programar_se_nao_atende(self, destino):
		""" Metodo responsável por programar o desvio de não atende no ramal que está sendo testado.
			Parâmetros:
				destino: Ramal interno do desvio"""

		estado = False
		try:
			cb_nao_atende = Select(self.driver.find_element_by_id("cbxRecDesSNADest"))
			cb_nao_atende.select_by_visible_text(str(destino))
			self.__salvar()
			estado = True
		except Exception:
			logging.exception('Erro na funcao __programar_se_nao_atende no arquivo ramal_desvios.py')
		finally:
			console("Programar Desvio Se Nao Atende", estado).outTest()


	def __programar_se_ocupado(self, destino):
		""" Metodo responsável por programar o desvio se ocupado no ramal que está sendo testado.
			Parâmetros:
				destino: Ramal interno do desvio"""

		estado = False
		try:
			cb_nao_atende = Select(self.driver.find_element_by_id("cbxRecDesSOCDest"))
			cb_nao_atende.select_by_visible_text(str(destino))
			self.__salvar()
			estado = True
		except Exception:
			logging.exception('Erro na funcao __programar_se_ocupado no arquivo ramal_desvios.py')
		finally:
			console("Programar Desvio Se Ocupado", estado).outTest()


	def __programar_siga_me(self, destino):
		""" Metodo responsável por programar o siga-me no ramal que está sendo testado.
			Parâmetros:
				destino: Ramal interno do siga-me"""

		estado = False
		try:
			cb_siga_me = Select(self.driver.find_element_by_id("cbxRecDesSigDest"))
			cb_siga_me.select_by_visible_text(str(destino))
			self.__salvar()
			estado = True
		except Exception:
			logging.exception('Erro na funcao __programar_siga_me_interno no arquivo ramal_desvios.py')
		finally:
			console("Programar Siga-me interno", estado).outTest()


	def __programar_entrante_direta(self, destino):
		""" Metodo responsável por programar o desvio se ocupado no ramal que está sendo testado.
			Parâmetros:
				destino: Ramal interno do desvio"""

		estado = False
		try:
			cb_nao_atende = Select(self.driver.find_element_by_id("cbxRecDesEntDir"))
			cb_nao_atende.select_by_visible_text(str(destino))
			self.__salvar()
			estado = True
		except Exception:
			logging.exception('Erro na funcao __programar_entrante_direta no arquivo ramal_desvios.py')
		finally:
			console("Programar Desvio Entrante Direta", estado).outTest()


	def make_test(self):
		""" Metodo responsável por chamar todos os testes pertencentes a tela Recebimento Geral > Desvios."""
		r_tools = RamaisTools(self.driver, self.file_log)

		# Acessa menu Ramal e o Ramal 204
		r_tools.menu_ramal()
		time.sleep(4)
		r_tools.acesso_ramal(203)
		time.sleep(1)

		self.__acessar_menu_desvios()
		time.sleep(1)
		self.__programar_tipo_desvios(True, True)
		time.sleep(2)

		self.__acessar_menu_desvios()
		time.sleep(1)
		self.__programar_siga_me(205)
		time.sleep(2)
		self.__acessar_menu_desvios()
		time.sleep(1)
		self.__programar_se_nao_atende(206)
		time.sleep(2)
		self.__acessar_menu_desvios()
		time.sleep(1)
		self.__programar_se_ocupado(207)
		time.sleep(2)
		self.__acessar_menu_desvios()
		time.sleep(1)
		self.__programar_entrante_direta(208)
		time.sleep(2)

		r_tools.menu_geral()

if __name__ == "__main__":
	path = os.getcwd()
	path = os.path.abspath('..')
	
	# Definicao do arquivo de log e inicializacao do driver
	file_log = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'
	driver = DriverTools(path).criar_driver_firefox()

	try:
		# Chamada do make_test
		RamalDesviosTest(driver, file_log).make_test()
	finally:
		driver.close()