# -*- coding: utf-8 -*-

import os
import sys
import logging
import time

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")


from output import console
from ramais_tools import RamaisTools
from driver_tools import DriverTools

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

""" Arquivo ramal_cadeado.py, este arquivo é utilizado para chamar todos os
	testes presentes na tela de Originação de Chamadas > Cadeado, ele é instanciado 
	pelo arquivo ramais.py ou então pode ser invocado via command line utilizando o comando
	python ramal_cadeado.py."""

class RamalCadeadoTest(object):
	""" Classe que contem todos os testes presentes na tela Originação de Chamadas > Cadeado.
		Parâmetros:
			driver: driver atual do navegador
			file_log: diretorio e nome do arquivo de log"""

	def __init__(self, driver, file_log):
		self.driver = driver
		self.file_log = file_log
		logging.basicConfig(filename=file_log,level=logging.ERROR,)


	def __salvar(self):
		""" Metodo responsavel por salvar as configurações do menu Originação de Chamadas > Cadeado"""
		estado = False
		try:
			self.driver.find_element_by_id("btnOriCadSalv").click()
			estado =  True
		except Exception:
			logging.exception('Erro na funcao salvar no arquivo ramal_cadeado.py')
		finally:
			console("Salvar - Cadeado", estado).outTest()


	def __acessar_menu_cadeado(self):
		""" Metodo responsável por entrar no menu Originação de Chamadas > Cadeado"""
		estado = False

		try:
			self.driver.find_element_by_id("boxOriCad").click()
			estado = True
		except Exception:
			logging.exception('Erro na funcao __acessar_menu_cadeado no arquivo ramal_cadeado.py')
		finally:
			console("Acessar menu cadeado", estado).outTest()


	def __definir_celular_internacional(self, habilitar):
		""" Metodo responsável por definir o cadeado celular internacional no ramal que está sendo testado.
			Parâmetros:
				habilitar: True = habilitar / False = desabilitar"""
		estado = False
		try:
			chk = self.driver.find_element_by_id("chkOriCadCelInt")
			lbl = self.driver.find_element(By.XPATH, "//label[@for='chkOriCadCelInt']")

			if habilitar:
				if not chk.is_selected():
					lbl.click()
			else:
				if chk.is_selected():
					lbl.click()

			estado = True
		except Exception:
			logging.exception('Erro na __definir_celular_internacional no arquivo ramal_cadeado.py')			
		finally:
			console("Cadeado celular internacional", estado).outTest()


	def __definir_celular_interurbano(self, habilitar):
		""" Metodo responsável por definir o cadeado celular interurbano no ramal que está sendo testado.
			Parâmetros:
				habilitar: True = habilitar / False = desabilitar"""
		estado = False
		try:
			chk = self.driver.find_element_by_id("chkOriCadCelInterurb")
			lbl = self.driver.find_element(By.XPATH, "//label[@for='chkOriCadCelInterurb']")

			if habilitar:
				if not chk.is_selected():
					lbl.click()
			else:
				if chk.is_selected():
					lbl.click()

			estado = True
		except Exception:
			logging.exception('Erro na __definir_celular_interurbano no arquivo ramal_cadeado.py')			
		finally:
			console("Cadeado celular interurbano", estado).outTest()


	def __definir_celular_regional(self, habilitar):
		""" Metodo responsável por definir o cadeado celular regional no ramal que está sendo testado.
			Parâmetros:
				habilitar: True = habilitar / False = desabilitar"""
		estado = False
		try:
			chk = self.driver.find_element_by_id("chkOriCadCelReg")
			lbl = self.driver.find_element(By.XPATH, "//label[@for='chkOriCadCelReg']")

			if habilitar:
				if not chk.is_selected():
					lbl.click()
			else:
				if chk.is_selected():
					lbl.click()

			estado = True
		except Exception:
			logging.exception('Erro na __definir_celular_regional no arquivo ramal_cadeado.py')			
		finally:
			console("Cadeado celular regional", estado).outTest()


	def __definir_celular_local(self, habilitar):
		""" Metodo responsável por definir o cadeado celular local no ramal que está sendo testado.
			Parâmetros:
				habilitar: True = habilitar / False = desabilitar"""
		estado = False
		try:
			chk = self.driver.find_element_by_id("chkOriCadCelLoc")
			lbl = self.driver.find_element(By.XPATH, "//label[@for='chkOriCadCelLoc']")

			if habilitar:
				if not chk.is_selected():
					lbl.click()
			else:
				if chk.is_selected():
					lbl.click()

			estado = True
		except Exception:
			logging.exception('Erro na __definir_celular_local no arquivo ramal_cadeado.py')			
		finally:
			console("Cadeado celular local", estado).outTest()


	def __definir_internacional(self, habilitar):
		""" Metodo responsável por definir o cadeado internacional no ramal que está sendo testado.
			Parâmetros:
				habilitar: True = habilitar / False = desabilitar"""
		estado = False
		try:
			chk = self.driver.find_element_by_id("chkOriCadInt")
			lbl = self.driver.find_element(By.XPATH, "//label[@for='chkOriCadInt']")

			if habilitar:
				if not chk.is_selected():
					lbl.click()
			else:
				if chk.is_selected():
					lbl.click()

			estado = True
		except Exception:
			logging.exception('Erro na __definir_internacional no arquivo ramal_cadeado.py')			
		finally:
			console("Cadeado internacional", estado).outTest()


	def __definir_interurbano(self, habilitar):
		""" Metodo responsável por definir o cadeado interurbano no ramal que está sendo testado.
			Parâmetros:
				habilitar: True = habilitar / False = desabilitar"""
		estado = False
		try:
			chk = self.driver.find_element_by_id("chkOriCadInterurb")
			lbl = self.driver.find_element(By.XPATH, "//label[@for='chkOriCadInterurb']")

			if habilitar:
				if not chk.is_selected():
					lbl.click()
			else:
				if chk.is_selected():
					lbl.click()

			estado = True
		except Exception:
			logging.exception('Erro na __definir_interurbano no arquivo ramal_cadeado.py')			
		finally:
			console("Cadeado interurbano", estado).outTest()

	def __definir_regional(self, habilitar):
		""" Metodo responsável por definir o cadeado regional no ramal que está sendo testado.
			Parâmetros:
				habilitar: True = habilitar / False = desabilitar"""
		estado = False
		try:
			chk = self.driver.find_element_by_id("chkOriCadReg")
			lbl = self.driver.find_element(By.XPATH, "//label[@for='chkOriCadReg']")

			if habilitar:
				if not chk.is_selected():
					lbl.click()
			else:
				if chk.is_selected():
					lbl.click()

			estado = True
		except Exception:
			logging.exception('Erro na __definir_regional no arquivo ramal_cadeado.py')			
		finally:
			console("Cadeado regional", estado).outTest()

	def __definir_local(self, habilitar):
		""" Metodo responsável por definir o cadeado local no ramal que está sendo testado.
			Parâmetros:
				habilitar: True = habilitar / False = desabilitar"""
		estado = False
		try:
			chk = self.driver.find_element_by_id("chkOriCadLoc")
			lbl = self.driver.find_element(By.XPATH, "//label[@for='chkOriCadLoc']")

			if habilitar:
				if not chk.is_selected():
					lbl.click()
			else:
				if chk.is_selected():
					lbl.click()

			estado = True
		except Exception:
			logging.exception('Erro na __definir_local no arquivo ramal_cadeado.py')			
		finally:
			console("Cadeado local", estado).outTest()

	def __definir_free_seat(self, habilitar):
		""" Metodo responsável por definir o cadeado free seat no ramal que está sendo testado.
			Parâmetros:
				habilitar: True = habilitar / False = desabilitar"""
		estado = False
		try:
			chk = self.driver.find_element_by_id("chkOriCadDeslFreSea")
			lbl = self.driver.find_element(By.XPATH, "//label[@for='chkOriCadDeslFreSea']")

			if habilitar:
				if not chk.is_selected():
					lbl.click()
			else:
				if chk.is_selected():
					lbl.click()

			estado = True
		except Exception:
			logging.exception('Erro na __definir_free_seat no arquivo ramal_cadeado.py')			
		finally:
			console("Cadeado free seat", estado).outTest()

	def make_test(self):
		""" Metodo responsável por chamar todos os testes pertencentes a tela Originação de Chamadas > cadeado. """
		r_tools = RamaisTools(self.driver, self.file_log)

		# Acessa menu Ramal e o Ramal 201 - Este deve ser um ramal Digital
		r_tools.menu_ramal()
		time.sleep(4)
		r_tools.acesso_ramal(202)
		time.sleep(1)

		self.__acessar_menu_cadeado()
		time.sleep(1)

		self.__definir_celular_internacional(True)
		self.__definir_celular_interurbano(False)
		self.__definir_celular_regional(True)
		self.__definir_celular_local(False)
		self.__definir_internacional(True)
		self.__definir_interurbano(False)
		self.__definir_regional(True)
		self.__definir_local(False)

		time.sleep(1)

		self.__salvar()
		time.sleep(2)

		r_tools.menu_geral()

if __name__ == "__main__":
	path = os.getcwd()
	path = os.path.abspath('..')
	
	# Definicao do arquivo de log e inicializacao do driver
	file_log = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'
	driver = DriverTools(path).criar_driver_firefox()

	try:
		# Chamada do make_test
		RamalCadeadoTest(driver, file_log).make_test()
	finally:
		driver.close()