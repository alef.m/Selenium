# -*- coding: utf-8 -*-

import os
import sys
import logging
import time

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")


from output import console
from ramais_tools import RamaisTools
from driver_tools import DriverTools
from loadConfig import configs

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

""" Arquivo ramal_fila.py, este arquivo é utilizado para chamar todos os
	testes presentes na tela de Recebimento Geral > Fila, ele é instanciado 
	pelo arquivo ramais.py ou então pode ser invocado via command line utilizando o comando
	python ramal_fila.py."""

class RamalFilaTest(object):
	""" Classe que contem todos os testes presentes na tela Recebimento Geral > Fila.
		Parâmetros:
			driver: driver atual do navegador
			file_log: diretorio e nome do arquivo de log"""

	def __init__(self, driver, file_log):
		self.driver = driver
		self.file_log = file_log
		logging.basicConfig(filename=file_log,level=logging.ERROR,)

	def __salvar(self):
		""" Metodo responsavel por salvar as configurações do menu Recebimento de Chamadas > Fila"""
		estado = False
		try:
			self.driver.find_element_by_id("btnRecFilSalv").click()
			estado =  True
		except Exception:
			logging.exception('Erro na funcao salvar no arquivo ramal_fila.py')
		finally:
			console("Salvar - Fila", estado).outTest()


	def __acessar_menu_fila(self):
		""" Metodo responsável por entrar no menu Recebimento de Chamadas > Fila"""
		estado = False

		try:
			self.driver.find_element_by_id("boxRecFila").click()
			estado = True
		except Exception:
			logging.exception('Erro na funcao acessar_menu_fila no arquivo ramal_fila.py')
		finally:
			console("Acessar menu Fila", estado).outTest()

	def __definir_fila(self, posicao):
		""" Metodo responsável por definir as configurações de fila.
			Parâmetros:
				posicao: quantidade de posições na fila"""
		estado = False
		try:
			cb_pos = Select(self.driver.find_element_by_id("cbxRecFilPos"))
			cb_sinalizacao = Select(self.driver.find_element_by_id("cbxRecFilSin"))
			cb_retida = Select(self.driver.find_element_by_id("chkRecFilChaRet"))

			chk_musica_ext = self.driver.find_element_by_id("chkRecFilMusExt")
			chk_prioriza_nao_atd = self.driver.find_element_by_id("chkRecFilPriNaoAtd")
			chk_ext = self.driver.find_element_by_id("chkRecFilExt")
			chk_int = self.driver.find_element_by_id("chkRecFilInt")

			lbl_musica_ext = self.driver.find_element(By.XPATH, "//label[@for='chkRecFilMusExt']")
			lbl_prioriza_nao_atd = self.driver.find_element(By.XPATH, "//label[@for='chkRecFilPriNaoAtd']")
			lbl_ext = self.driver.find_element(By.XPATH, "//label[@for='chkRecFilExt']")
			lbl_int = self.driver.find_element(By.XPATH, "//label[@for='chkRecFilInt']")

			cb_tempo_interna_diurno = Select(self.driver.find_element_by_id("chkRecFilNaoAtdIntTmpTur1"))
			cb_atendedor_interna_diurno = Select(self.driver.find_element_by_id("chkRecFilNaoAtdIntTur1"))
			cb_tempo_interna_noturno = Select(self.driver.find_element_by_id("chkRecFilNaoAtdIntTmpTur2"))
			cb_atendedor_interna_noturno = Select(self.driver.find_element_by_id("chkRecFilNaoAtdIntTur2"))

			cb_tempo_externa_diurno = Select(self.driver.find_element_by_id("chkRecFilNaoAtdExtTmpTur1"))
			cb_atendedor_externa_diurno = Select(self.driver.find_element_by_id("chkRecFilNaoAtdExtTur1"))
			cb_tempo_externa_noturno = Select(self.driver.find_element_by_id("chkRecFilNaoAtdExtTmpTur2"))
			cb_atendedor_externa_noturno = Select(self.driver.find_element_by_id("chkRecFilNaoAtdExtTur2"))

			cb_pos.select_by_visible_text(str(posicao))
			cb_sinalizacao.select_by_visible_text("Sem BIP")

			lbl_musica_ext.click()
			lbl_prioriza_nao_atd.click()
			lbl_ext.click()
			lbl_int.click()

			cb_tempo_interna_diurno.select_by_visible_text("10")
			cb_atendedor_interna_diurno.select_by_visible_text("206")
			cb_tempo_interna_noturno.select_by_visible_text("10")
			cb_atendedor_interna_noturno.select_by_visible_text("206")

			cb_tempo_externa_diurno.select_by_visible_text("10")
			cb_atendedor_externa_diurno.select_by_visible_text("206")
			cb_tempo_externa_noturno.select_by_visible_text("10")
			cb_atendedor_externa_noturno.select_by_visible_text("206")

			estado = True
		except Exception:
			logging.exception('Erro na funcao salvar no arquivo ramais_pre_atendimento.py')
			pass
		finally:
			console("Definir fila", estado).outTest()

	def make_test(self):
		""" Metodo responsável por chamar todos os testes pertencentes a tela Recebimento Geral > Fila."""
		r_tools = RamaisTools(self.driver, self.file_log)

		# Acessa menu Ramal e o Ramal 204
		r_tools.menu_ramal()
		time.sleep(4)
		r_tools.acesso_ramal(204)
		time.sleep(1)

		self.__acessar_menu_fila()
		time.sleep(2)
		self.__definir_fila(4)
		time.sleep(1)
		self.__salvar()

		r_tools.menu_geral()

if __name__ == "__main__":
	path = os.getcwd()
	path = os.path.abspath('..')
	
	# Definicao do arquivo de log e inicializacao do driver
	file_log = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'
	driver = DriverTools(path).criar_driver_firefox()

	try:
		# Chamada do make_test
		RamalFilaTest(driver, file_log).make_test()
	finally:
		driver.close()