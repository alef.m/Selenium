# -*- coding: utf-8 -*-

import os
import sys
import logging
import time

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")


from output import console
from ramais_tools import RamaisTools
from driver_tools import DriverTools

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

""" Arquivo ramal_linha_executiva.py, este arquivo é utilizado para chamar todos os
	testes presentes na tela de Recebimento Geral > Linha Executiva, ele é instanciado 
	pelo arquivo ramais.py ou então pode ser invocado via command line utilizando o comando
	python ramal_linha_executiva.py."""

class RamalLinhaExecutivaTest(object):
	""" Classe que contem todos os testes presentes na tela Recebimento Geral > Linha Executiva.
		Parâmetros:
			driver: driver atual do navegador
			file_log: diretorio e nome do arquivo de log"""

	def __init__(self, driver, file_log):
		self.driver = driver
		self.file_log = file_log
		logging.basicConfig(filename=file_log,level=logging.ERROR,)


	def __salvar(self):
		""" Metodo responsavel por salvar as configurações do menu Recebimento de Chamadas > Linha Executiva"""
		estado = False
		try:
			self.driver.find_element_by_id("btnRecLinExeSalv").click()
			estado =  True
		except Exception:
			logging.exception('Erro na funcao salvar no arquivo ramal_linha_executiva.py')
		finally:
			console("Salvar - Linha Executiva", estado).outTest()


	def __acessar_menu_linha_executiva(self):
		""" Metodo responsável por entrar no menu Recebimento de Chamadas > Linha Executiva"""
		estado = False

		try:
			self.driver.find_element_by_id("boxRecLinExec").click()
			estado = True
		except Exception:
			logging.exception('Erro na funcao __acessar_menu_linha_executiva no arquivo ramal_linha_executiva.py')
		finally:
			console("Acessar menu Linha Executiva", estado).outTest()


	def __definir_linha_executiva(self, tipo):
		""" Metodo responsável por definiri o tipo de linha executiva que será utilizado no ramal que está sendo testado.
			Parâmetros:
				tipo: 1 = Não Programado / 2 = Com senha / 3 = Sem senha"""
		estado = False
		nome = ""
		try:
			if(tipo == 1):
				rbtn_linha_executiva = self.driver.find_element(By.XPATH, "//label[@for='chkRecLinExeProg']").click()
				nome = "nao programado"
			else:
				if(tipo == 2):
					rbtn_linha_executiva = self.driver.find_element(By.XPATH, "//label[@for='chkRecLinExeComSen']").click()
					nome = "com senha"
				else:
					if(tipo == 3):
						rbtn_linha_executiva = self.driver.find_element(By.XPATH, "//label[@for='chkRecLinExeSemSen']").click()
						nome = "sem senha"

			self.__salvar()
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_linha_executiva no arquivo ramal_linha_executiva.py')
		finally:
			console("Definir Linha Executiva " + nome, estado).outTest()


	def make_test(self):
		""" Metodo responsável por chamar todos os testes pertencentes a tela Recebimento Geral > Linha Executiva."""
		r_tools = RamaisTools(self.driver, self.file_log)

		# Acessa menu Ramal e o Ramal 204
		r_tools.menu_ramal()
		time.sleep(4)
		r_tools.acesso_ramal(203)
		time.sleep(1)

		self.__acessar_menu_linha_executiva()
		time.sleep(1)
		self.__definir_linha_executiva(1)
		time.sleep(2)
		r_tools.menu_geral()
		time.sleep(1)

		r_tools.menu_ramal()
		time.sleep(4)
		r_tools.acesso_ramal(204)
		time.sleep(1)

		self.__acessar_menu_linha_executiva()
		time.sleep(1)
		self.__definir_linha_executiva(2)
		time.sleep(2)
		r_tools.menu_geral()
		time.sleep(1)

		r_tools.menu_ramal()
		time.sleep(4)
		r_tools.acesso_ramal(205)
		time.sleep(1)

		self.__acessar_menu_linha_executiva()
		time.sleep(1)
		self.__definir_linha_executiva(3)
		time.sleep(2)
		r_tools.menu_geral()


if __name__ == "__main__":
	path = os.getcwd()
	path = os.path.abspath('..')
	
	# Definicao do arquivo de log e inicializacao do driver
	file_log = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'
	driver = DriverTools(path).criar_driver_firefox()

	try:
		# Chamada do make_test
		RamalLinhaExecutivaTest(driver, file_log).make_test()
	finally:
		driver.close()