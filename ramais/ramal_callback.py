# -*- coding: utf-8 -*-

import os
import sys
import logging
import time

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")


from output import console
from ramais_tools import RamaisTools
from driver_tools import DriverTools
from loadConfig import configs

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

""" Arquivo ramal_callback.py, este arquivo é utilizado para chamar todos os
	testes presentes na tela de Recebimento Geral > Callback, ele é instanciado 
	pelo arquivo ramais.py ou então pode ser invocado via command line utilizando o comando
	python ramal_callback.py."""

class RamalCallbackTest(object):
	""" Classe que contem todos os testes presentes na tela Recebimento Geral > Callback.
		Parâmetros:
			driver: driver atual do navegador
			file_log: diretorio e nome do arquivo de log"""

	def __init__(self, driver, file_log):
		self.driver = driver
		self.file_log = file_log
		logging.basicConfig(filename=file_log,level=logging.ERROR,)


	def __salvar(self):
		""" Metodo responsavel por salvar as configurações do menu Recebimento de Chamadas > Callback"""
		estado = False
		try:
			self.driver.find_element_by_id("btnRecCalSalv").click()
			estado =  True
		except Exception:
			logging.exception('Erro na funcao salvar no arquivo ramal_callback.py')
		finally:
			console("Salvar - Callback", estado).outTest()


	def __acessar_menu_callback(self):
		""" Metodo responsável por entrar no menu Recebimento de Chamadas > Callback"""
		estado = False

		try:
			self.driver.find_element_by_id("boxRecCall").click()
			estado = True
		except Exception:
			logging.exception('Erro na funcao __acessar_menu_callback no arquivo ramal_callback.py')
		finally:
			console("Acessar menu Callback", estado).outTest()


	def __programar_callback(self, numExt, ramal):
		""" Metodo responsável por programar o Callback no ramal que está em testes."""
		estado = False
		try:
			txt_num_ext = self.driver.find_element_by_id("txtRecCalNumExt")
			cb_ramal = Select(self.driver.find_element_by_id("cbxRecCalRam"))

			txt_num_ext.clear()
			time.sleep(1)
			txt_num_ext.send_keys(str(numExt))
			cb_ramal.select_by_visible_text(str(ramal))

			self.__salvar()
			estado = True
		except Exception:
			logging.exception('Erro na funcao __programar_callback no arquivo ramal_callback.py')
		finally:
			console("Programar Callback", estado).outTest()


	def make_test(self):
		""" Metodo responsável por chamar todos os testes pertencentes a tela Recebimento Geral > Callback."""
		r_tools = RamaisTools(self.driver, self.file_log)

		# Acessa menu Ramal e o Ramal 204
		r_tools.menu_ramal()
		time.sleep(4)
		r_tools.acesso_ramal(203)
		time.sleep(1)

		self.__acessar_menu_callback()
		time.sleep(2)
		self.__programar_callback(98369511, 206)
		time.sleep(2)

		r_tools.menu_geral()


if __name__ == "__main__":
	path = os.getcwd()
	path = os.path.abspath('..')
	
	# Definicao do arquivo de log e inicializacao do driver
	file_log = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'
	driver = DriverTools(path).criar_driver_firefox()

	try:
		# Chamada do make_test
		RamalCallbackTest(driver, file_log).make_test()
	finally:
		driver.close()