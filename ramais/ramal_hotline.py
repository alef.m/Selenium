# -*- coding: utf-8 -*-

import os
import sys
import logging
import time

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")


from output import console
from ramais_tools import RamaisTools
from driver_tools import DriverTools

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

""" Arquivo ramal_hotline.py, este arquivo é utilizado para chamar todos os
	testes presentes na tela de Originação de Chamadas > Hotline, ele é instanciado 
	pelo arquivo ramais.py ou então pode ser invocado via command line utilizando o comando
	python ramal_hotline.py."""

class RamalHotlineTest(object):
	""" Classe que contem todos os testes presentes na tela Originação de Chamadas > Hotline.
		Parâmetros:
			driver: driver atual do navegador
			file_log: diretorio e nome do arquivo de log"""


	def __init__(self, driver, file_log):
		self.driver = driver
		self.file_log = file_log
		logging.basicConfig(filename=file_log,level=logging.ERROR,)


	def __salvar(self):
		""" Metodo responsavel por salvar as configurações do menu Originação de Chamadas > Hotline"""
		estado = False
		try:
			self.driver.find_element_by_id("btnOriHotLinSalv").click()
			estado =  True
		except Exception:
			logging.exception('Erro na funcao salvar no arquivo ramal_hotline.py')
		finally:
			console("Salvar - Hotline", estado).outTest()


	def __acessar_menu_hotline(self):
		""" Metodo responsável por entrar no menu Originação de Chamadas > Hotline"""
		estado = False

		try:
			self.driver.find_element_by_id("boxOrigHot").click()
			estado = True
		except Exception:
			logging.exception('Erro na funcao __acessar_menu_hotline no arquivo ramal_hotline.py')
		finally:
			console("Acessar menu hotline", estado).outTest()


	def __definir_destino(self, destino):
		""" Metodo responsável por definir o destino do hotline que sera programado no ramal que esta sendo testado
			Parâmetros:
				destino: numero do ramal destino / 0 = Número Externo."""
		estado = False
		try:
			cb_dst = Select(self.driver.find_element_by_id("cbxOriHotLinDes"))

			if(destino == 0):
				cb_dst.select_by_visible_text("Número Externo")
			else:
				cb_dst.select_by_visible_text(str(destino))

			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_destino no arquivo ramal_hotline.py')
		finally:
			console("Definir destino hotline", estado).outTest()


	def __definir_rota_filial(self, rota):
		""" Metodo responsável por definir a rota do hotline que sera programado no ramal que esta sendo testado
			Parâmetros:
				rota: 0 = Rota automática."""
		estado = False
		try:
			cb_rota = self.driver.find_element_by_id("cbxOriHotLinRot")

			if cb_rota.is_enabled():
				
				cb_rota = Select(cb_rota)
				if(rota == 0):
					cb_rota.select_by_visible_text("Rota automática")
				else:
					# Este tipo de rota não foi definido por não saber o padrão que será utilizado pelo programador
					print "Definir tipo de rota @@@@@@@@"

				estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_rota_filial no arquivo ramal_hotline.py')
		finally:
			console("Definir rota hotline", estado).outTest()


	def __definir_numero_ext(self, numero):
		""" Metodo responsável por definir o numero externo que será configurado no hotline do ramal que está sendo testado
			Parâmetros:
				numero: Numero externo utilizado no hotline"""
		estado = False
		try:	
			txt_ext = self.driver.find_element_by_id("txtOriHotLinNumExt")
			if txt_ext.is_enabled():
				txt_ext.clear()
				txt_ext.send_keys(str(numero))
				estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_numero_ext no arquivo ramal_hotline.py')
		finally:
			console("Definir numero externo hotline", estado).outTest()


	def __definir_ramal_emerg(self, habilitar):
		""" Metodo responsável por definir ramal de emergencia.
			Parâmetros:
				habilitar: True = habilitado / False = desabilitado"""
		estado = False
		try:
			chk_emerg = self.driver.find_element_by_id("chkOriHotLinRamEme")
			if chk_emerg.is_enabled():
				lbl_emerg = self.driver.find_element(By.XPATH, "//label[@for='chkOriHotLinRamEme']")

				if habilitar:		
					if not chk_emerg.is_selected():
						lbl_emerg.click()
				else:
					if chk_emerg.is_selected():
						lbl_emerg.click()
				estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_ramal_emerg no arquivo ramal_hotline.py')
		finally:
			console("Definir ramal emergencia hotline", estado).outTest()


	def __definir_tempo_hotline(self, tempo):
		""" Metodo responsável por definir o tempo de hotline do ramal que está sendo testado
			Parâmetros:
				tempo: 0 = Imediato / 1-7 - tempo do hotline"""
		estado = False
		try:
			cb_tempo = Select(self.driver.find_element_by_id("cbxOriHotLinTem"))
			if (tempo == 0):
				cb_tempo.select_by_visible_text("Imediato")
			else:
				cb_tempo.select_by_visible_text(str(tempo))
			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_tempo_hotline no arquivo ramal_hotline.py')

		finally:
			console("Definir tempo hotline", estado).outTest()


	def make_test(self):
		""" Metodo responsável por chamar todos os testes pertencentes a tela Originação de Chamadas > Hotline. """
		r_tools = RamaisTools(self.driver, self.file_log)

		# Acessa menu Ramal e o Ramal 201 - Este deve ser um ramal Digital
		r_tools.menu_ramal()
		time.sleep(4)
		r_tools.acesso_ramal(202)
		time.sleep(1)

		self.__acessar_menu_hotline()
		time.sleep(1)

		self.__definir_destino(0)
		self.__definir_rota_filial(0)
		self.__definir_numero_ext(98369511)
		self.__definir_ramal_emerg(False)
		self.__definir_tempo_hotline(0)

		time.sleep(1)
		self.__salvar()
		time.sleep(2)

		r_tools.menu_geral()

if __name__ == "__main__":
	path = os.getcwd()
	path = os.path.abspath('..')
	
	# Definicao do arquivo de log e inicializacao do driver
	file_log = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'
	driver = DriverTools(path).criar_driver_firefox()

	try:
		# Chamada do make_test
		RamalHotlineTest(driver, file_log).make_test()
	finally:
		driver.close()