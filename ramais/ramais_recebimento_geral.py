# -*- coding: utf-8 -*-

import os
import sys
import logging
import time

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")

from driver_tools import DriverTools
from datetime import datetime
from selenium import webdriver
from output import console
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from ramais_tools import RamaisTools

class RecebimentoGeralTest(object):
	"""Classe que contém todos os testes referentes ao menu Ramal>Recebimento de Chamadas> Geral
	Parâmetros:
		Habilitar = Se deve habilitar o checkbox (true) ou se deve desabilitar o checkbox (false)"""
	def __init__(self, driver, file_log):
		self.driver = driver
		self.file_log = file_log
		logging.basicConfig(filename=file_log,level=logging.ERROR,)

	def __definir_nao_perturbe(self, habilitar):
		"""Função especifica para definir o checkbox Nao Perturbe dentro do menu Ramal>Recebimento de Chamadas>Geral"""
		estado = False
		try:
			chk_nao_perturbe = self.driver.find_element_by_id("chkRecGerNaoPert")
			if habilitar:
				if not chk_nao_perturbe.is_selected():
					self.driver.find_element(By.XPATH, "//label[@for='chkRecGerNaoPert']").click()

		
			else:
				if chk_nao_perturbe.is_selected():
					self.driver.find_element(By.XPATH, "//label[@for='chkRecGerNaoPert']").click()
			estado = True


		except Exception:
			logging.exception('Erro na funcao habilita nao perturbe do arquivo ramais_recebimento_geral.py')
			pass		
		finally:
			console("Programa Nao Perturbe", estado).outTest()



	
	"""Função especifica para acessar o menu Ramal>Recebimento de chamadas>Geral"""
	
	def __acessar_menu_ramal_recebimento_geral(self):
		
		estado = False
		try:
			# Acesso ao menu Geral
			self.driver.find_element_by_id("boxRecGer").click()

			estado = True
		#Print na tela de saida sobre status de acesso ao menu Recebimento Geral dentro do ramal
		except Exception:
			logging.exception('Erro na funcao acessa_menu_ramal_recebimento_geral no arquivo ramais_recebimento_geral.py')
			pass		
		finally:
			console("Acessar menu - Ramal Recebimento Geral", estado).outTest()



	def __salvar(self):
		estado = False
		"""Função específica para salvar os parâmetros alterados no menu Ramal>Recebimento de Chamadas>Geral"""
		try:
			# Salvar configuracao
			self.driver.find_element_by_id("btnRecGerSalv").click()
			estado = True
		except Exception:
			logging.exception('Erro na funcao salvar no arquivo ramal_recebimento_geral.py')
			pass
		finally:
			console("Salvar - Recebimento Geral", estado).outTest()

		# Funcao que habilita ou desabilita checkbox Recebe e Captura Chamada no ramal
		# Habilitar = Se deve habilitar (true) ou se deve desabilitar (false) o checkbox
	
	def __definir_recebe_captura_chamada(self, habilitar):
		"""Função especifica para definir o checkbox Recebe e Captura Chamada no menu Ramal > Recebimento de Chamadas> Geral"""
		estado = False
		try:
			#Procura o checkbox Recebe e Captura chamada
			chk_recebe_captura_chamada = self.driver.find_element_by_id("chkRecGerCapExt")
			if habilitar:
				#Caso tenha que habilitar e esteja habilitado deixa como esta
				if not chk_recebe_captura_chamada.is_selected():
					self.driver.find_element(By.XPATH, "//label[@for='chkRecGerCapExt']").click()
								
			else:
				#Caso tenha que desabilitar e esteja habilitado clica no checkbox
				if chk_recebe_captura_chamada.is_selected():
					self.driver.find_element_by_id(By.XPATH, "//label[@for='chkRecGerCapExt']").click()
			estado = True

				

		except Exception:
			logging.exception('Erro na funcao recebe_captura_chamada do arquivo ramais_recebimento_geral.py')
			pass		
		#Print na tela de saida sobre status do teste programar Recebe e Captura chamada no ramal
		finally:
			console("Progama Recebe e Captura Chamada", estado).outTest()



		# Funcao que habilita ou desabilita checkbox E Capturado no ramal
		# Habilitar = Se deve habilitar (true) ou se deve desabilitar (false) o checkbox
	def __definir_e_capturado(self, habilitar):
		"""Função especifica para definir o checkbox É Capturado  no menu Ramal > Recebimento de Chamadas> Geral"""
		estado = False
		try:
			#Procura o botao E Capturado
			chk_e_capturado = self.driver.find_element_by_id("chkRecGerECap")
			if habilitar:
				#Caso tenha que habilitar e esteja habilitado deixa como esta
				if not chk_e_capturado.is_selected():
					self.driver.find_element(By.XPATH, "//label[@for='chkRecGerECap']").click()

			else:
				#Caso tenha que desabilitar e esteja habilitado clica no checkbox
				if chk_e_capturado.is_selected():
					self.driver.find_element_by_id(By.XPATH, "//label[@for='chkRecGerECap']").click()
				
			estado = True
				

		except Exception:
			logging.exception('Erro na funcao e_capturado do arquivo ramais_recebimento_geral.py')
			pass		
		#Print na tela de saida sobre status do teste programar E Capturado no ramal
		finally:
			console("Progama E Capturado", estado).outTest()
			

	# Funcao que habilita ou desabilita checkbox Direto da Disa no ramal
	# Habilitar = Se deve habilitar (true) ou se deve desabilitar (false) o checkbox
	def __definir_direto_da_disa(self, habilitar):
		"""Função especifica para definir o checkbox Direta da Disa no menu Ramal > Recebimento de Chamadas> Geral"""

		estado = False
		try:
			#Procura o checkbox Direto da Disa
			chk_direto_da_disa = self.driver.find_element_by_id("chkRecGerDirDisa")
			if habilitar:
				#Caso tenha que habilitar e esteja habilitado deixa como esta
				if not chk_direto_da_disa.is_selected():
					self.driver.find_element(By.XPATH, "//label[@for='chkRecGerDirDisa']").click()
				
			else:
				#Caso tenha que desabilitar e esteja habilitado clica no checkbox
				if chk_direto_da_disa.is_selected():
					self.driver.find_element_by_id(By.XPATH, "//label[@for='chkRecGerDirDisa']").click()

			estado = True
				

		except Exception:
			logging.exception('Erro na funcao direto_da_disa do arquivo ramais_recebimento_geral.py')
			pass		
		#Print na tela de saida sobre status do teste programar chamada direto da disa
		finally:
			console("Progama Recebimento de Chamada Direto da Disa no Ramal", estado).outTest()


	def __definir_a_cobrar(self, habilitar):
		"""Função especifica para definir o checkbox A Cobrar no menu Ramal > Recebimento de Chamadas> Geral"""

		estado = False
		try:
			chk_a_cobrar = self.driver.find_element_by_id("chkRecGerACob")
			if habilitar:
				if not chk_a_cobrar.is_selected():
					self.driver.find_element(By.XPATH, "//label[@for='chkRecGerACob']").click()

			else:
				if chk_a_cobrar.is_selected():
					self.driver.find_element(By.XPATH, "//label[@for='chkRecGerACob']").click()

			estado = True

		except Exception:
			logging.exception('Erro na funcao __a_cobrar do arquivo ramais_recebimento_geral.py')
			pass
		finally:
			console("Programa A Cobrar no Ramal", estado).outTest()


	def __definir_filtro_ani(self, habilitar):
		"""Funcao especifica para definir o checkbox Filtro Ani no menu Ramal > Recebimento de Chamadas> Geral"""

		estado = False
		try:
			chk_filtro_ani = self.driver.find_element_by_id("chkRecGerFilANI")
			if habilitar:
				if not chk_filtro_ani.is_selected():
					self.driver.find_element(By.XPATH, "//label[@for='chkRecGerFilANI']")

			else: 
				if chk_filtro_ani.is_selected():
					self.driver.find_elembent(By.XPATH, "//label[@for='chkRecGerFilANI']")

			estado = True

		except Exception:
			logging.exception('Erro na funcao __filtro_ani do arquivo ramais_recebimento_geral.py')
			pass
		finally:
			console("Programa Filtro Ani", estado).outTest()

	def __definir_atende_por_comando(self, habilitar):
		"""Funcao especifica para definir o checkbox Atende por Comando no menu Ramal > Recebimento de Chamadas> Geral"""

		estado = False
		try:
			chk_atende_por_comando = self.driver.find_element_by_id("chkRecGerAtdCom")
			if habilitar:
				if not chk_atende_por_comando.is_selected:
					self.driver.find_element(By.XPATH, "//label[@for='chkRecGerAtdCom']")

			else:
				if chk_atende_por_comando.is_selected():
					self.driver.find_element(By.XPATH, "//label[@for='chkRecGerAtdCom']")

			estado = True

		except Exception:
			logging.exception('Erro na funcao __atende_por_comando do arquivo ramais_recebimento_geral.py')
			pass
		finally:
			console("Programa Atende por Comando", estado).outTest()



	

	def __definir_duracao_maxima(self):
		"""Função Específica para definir o combobox Duração Máxima no menu Ramal > Recebimento de Chamadas > Geral"""
		estado = False
		try:
			cbDuracao = self.driver.find_element_by_id("chkRecGerDurMax")
			if cbDuracao.is_enabled():
				select_cb_Duracao = Select(cbDuracao)
				select_cb_Duracao.select_by_visible_text('9')
				estado = True

		except Exception:
			logging.exception('Erro na Duracao Maxima no arquivo ramal_recebimento_geral.py')
			pass				
		finally:
			console("Programa duracao maxima", estado).outTest()


	def definir_interna_condicionada(self, habilitar_diu, habilitar_not):
		estado = False
		try:

			chk_diu = self.driver.find_element_by_id("chkRecGerIntCond1")
			lbl_diu = self.driver.find_element(By.XPATH, "//label[@for='chkRecGerIntCond1']")

			chk_not = self.driver.find_element_by_id("chkRecGerIntCond2")
			lbl_not = self.driver.find_element(By.XPATH, "//label[@for='chkRecGerIntCond2']")

			if habilitar_diu:
				if not chk_diu.is_selected():
					lbl_diu.click()

			else:
				if chk_diu.is_selected():
					lbl_diu.click()


			if habilitar_not:
				if not chk_not.is_selected():
					lbl_not.click()

			else:
				if chk_diu.is_selected():
					lbl_diu.click()

			estado = True
		except Exception:
			logging.exception('Erro na funcao __definir_interna_condicionada no arquivo ramal_recebimento_geral.py')
		finally:
			console("Definir chamada interna condicionada", estado).outTest()

			
	def make_test(self):
		r_tools = RamaisTools(self.driver, self.file_log)
		
		#Acessa menu ramal e o ramal 200z
		r_tools.menu_ramal()
		time.sleep(5)
		r_tools.acesso_ramal(200)
		#Acessa menu ramal Recebimento Geral
		self.__acessar_menu_ramal_recebimento_geral()
		time.sleep(1)
		
		#testa checkbox Nao Perturbe   True = Habilita  False = Desabilita
		self.__definir_nao_perturbe(True)

		#Testa Cehckbox Recebe e captura chamada  True = Habilita  False = Desabilita
		self.__definir_recebe_captura_chamada(True)

		#Testa checkbox e capturado    True = Habilita  False = Desabilita
		self.__definir_e_capturado(True)
			
		#Testa checkbox direto da disa
		self.__definir_direto_da_disa(True)
	
		#Testa checkbox A Cobrar
		self.__definir_a_cobrar(True)
	
		#Testa checkbox Filtro ANI
		self.__definir_filtro_ani(True)

		#Testa checkbox Atende por Comando
		self.__definir_atende_por_comando(True)

		#Testa checkbox Interna Condicionada diurno e noturno
		self.definir_interna_condicionada(True, True)


		#Testa combobox Duracao Maxima
		self.__definir_duracao_maxima()
		self.__salvar()

		#Volta para o menu geral
		r_tools.menu_geral()


if __name__ == "__main__":
	path = os.getcwd()
	path = os.path.abspath('..')
	
	# Definicao do arquivo de log e inicializacao do driver
	file_log = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'
	driver = DriverTools(path).criar_driver_firefox()

	try:
		# Chamada do make_test
		RecebimentoGeralTest(driver, file_log).make_test()
	finally:
		driver.close()





