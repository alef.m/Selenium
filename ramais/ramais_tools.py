# -*- coding: utf-8 -*-

import os
import sys
import logging

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")


import time
from output import console
from driver_tools import DriverTools
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC        


""" Arquivo ramais_tools.py, este arquivo é utilizado como ferramenta para ações
	que são comum para todos os testes pertencentes ao modulo de ramais como por
	exemplo a ação de acessar um determinado ramal. """

class RamaisTools(object):
	""" Classe que contem metodos utilizados por todos os testes do modulo de ramais.
		Parâmetros:
			driver: driver atual do navegador
			file_log: diretorio e nome do arquivo de log"""

	def __init__(self, driver, file_log):
		self.driver = driver
		self.file_log = file_log
		logging.basicConfig(filename=file_log,level=logging.ERROR,)

	def pesquisar_ramal(self, numero):
		""" Pesquisa um determinado ramal na tela de listagem de ramais. 
			Prâmetros:
				numero: numero a ser pesquisado"""

		estado = False
		try:
			pesqTxt = self.driver.find_element(By.XPATH, "//input[@class='input input-search ng-pristine ng-untouched ng-valid ng-empty']")

			pesqTxt.send_keys(numero)

			estado = True
		except Exception:
			logging.exception('Erro na funcao pesquisar_ramal no arquivo ramais_tools.py')
			pass
		finally:
			console("Pesquisa de Ramal", estado).outTest()



	def menu_geral(self):
		""" Retorna para o Menu Geral (Menu inicial do equipamento após efetuar o login). """
		try:
			time.sleep(2)
			# Clica no menu Admin / Matriz para voltar pra tela inicial
			menu = self.driver.find_element(By.XPATH, "//li[@class='dropdown']//a[@class='dropdown-toggle']").click()

		except Exception:
			logging.exception('Erro na funcao menu_geral no arquivo ramais_tools.py')
			pass

	def menu_ramal(self):
		""" Metodo responsável por acessar o menu ramal (Menu que contem a lista de todos os ramais presentes no PABX). """
		try:
			# Acessoa menu Ramais pelo xPath
			menu = self.driver.find_element(By.XPATH, "//li[@id='mnuRamais']")

			# Comando actions para mover o mouse 
			actions = ActionChains(self.driver)
			actions.move_to_element(menu)
			actions.perform()

			# Sleep de seguranca para aparecer o submenu
			time.sleep(1)

			# Acessando o submenu de ramais
			submenu = self.driver.find_element(By.XPATH, "//li[@id='mnuRamais']//a[@id='mnu_rRamais']")
			submenu.click()
		except Exception:
			logging.exception('Erro na funcao menu_ramal no arquivo ramais_tools.py')
			pass


	def is_element_present(self, id_elemento):
		""" Metodo responsável por checar se um determinado elemento está presente na tela. 
			Parâmetros:
				id_elemento: ID do elemento que será buscado na tela."""
		try:
			self.driver.find_element_by_id(id_elemento)
			return True
		except NoSuchElementException:
			return False

	# Metodo responsavel por acessar um ramal pelo seu numero
	def acesso_ramal(self, numero):
		""" Metodo responsável por acessar um determinado ramal.
			Prâmetros:
				numero: numero do ramal que será acessado"""
		try:
			# Inicia o estado do teste como falha
			estado = False

			# Concatenacao de string com int
			strAux = "tblRamal"+str(numero)

			# Timer de seguranca para aparecer a listagem de ramal
			time.sleep(3)

			# Se o numero passado nao e encontrado
			if(not self.is_element_present(strAux)):
				if(numero == 100):
					numero += 100
					strAux = "tblRamal"+str(numero)
				else:
					numero -= 100
					strAux = "tblRamal"+str(numero)

			# Click no ramal selecionado
			self.driver.find_element_by_id(strAux).click()

			# Verifica se o ramal foi acessador corretamente
			if ("configurar ramal") in self.driver.page_source:
				estado = True

		except Exception:
			logging.exception('Erro na funcao acesso_ramal no arquivo ramais_tools.py')
		finally:
			console("Acesso ramal " + str(numero), estado).outTest()


