import time
from output import console
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys

class LoginTest(object):

	def __init__(self, driver, login, pwd):
		self.driver = driver
		self.login = login
		self.pwd = pwd
	

	def invalid_login(self):
		# Inicia o estado do teste como falha
		estado = False

		time.sleep(1)
		# Limpando os campos de nome e senha
		self.driver.find_element_by_id("logUsu").clear()
		senha = self.driver.find_element_by_id("logSen").clear()

		# Procura o campo de nome de usuario pelo seu ID
		user = self.driver.find_element_by_id("logUsu")

		# Preenche o campo com o nome de usuario
		user.send_keys(self.login)

		# Procura o campo de senha pelo seu ID
		passwd = self.driver.find_element_by_id("logSen")

		# Preenche o campo com a senha e mais dois caracteres a mais para forcar a senha incorreta
		passwd.send_keys(self.pwd + "xx")

		# Click no botao Entrar
		self.driver.find_element_by_id("logBtnEnt").click()

		# Validacao do teste, caso senha invalida teste OK
		if ("MARCA") in self.driver.page_source:
			estado = True

		# Fechando pop-up de senha invalida
		# self.driver.find_element_by_id("ext-gen700").click()
		# self.driver.find_element_by_id("ext-gen700").click()

		# Limpando os campos de nome e senha
		# self.driver.find_element_by_id("logusu").clear()
		# senha = self.driver.find_element_by_id("logSen").clear()

		# Saida do teste no console
		console("Login Invalido", estado).outTest()

		time.sleep(2)
		

	def valid_login(self):
		# Inicia o estado do teste como falha
		estado = False

		time.sleep(1)
		# Limpando os campos de nome e senha
		self.driver.find_element_by_id("logUsu").clear()
		senha = self.driver.find_element_by_id("logSen").clear()

		time.sleep(1)

		# Procura o campo de nome de usuario pelo seu ID
		user = self.driver.find_element_by_id("logUsu")

		# Preenche o campo com o nome de usuario
		user.send_keys(self.login)

		# Procura o campo de senha pelo seu ID
		passwd = self.driver.find_element_by_id("logSen")
		# Preenche o campo com a senha
		passwd.send_keys(self.pwd)

		# Click no botao Entrar
		self.driver.find_element_by_id("logBtnEnt").click()

		# Validacao do teste de login valido
		if ("Matriz") in self.driver.page_source:
			# self.driver.find_element_by_id("ext-gen1466").click()
			estado = True

		# Saida do teste no console
		console("Login Valido", estado).outTest()

		time.sleep(2)





