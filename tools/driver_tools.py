import os
import sys

path = os.getcwd()
path = os.path.abspath('..')
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/configuracoes")

from selenium import webdriver
from loadConfig import configs
from login import LoginTest


class DriverTools(object):

	def __init__(self, path):
		self.path = path
		# Carrega as configuracoes do arquivo config.txt
		self.host = configs().load_config(self.path, 'host')
		self.user = configs().load_config(self.path, 'user')
		self.pwd = configs().load_config(self.path, 'pwd')

	# Metodo responsavel por criar e retornar um driver Firefox do Selenium
	def criar_driver_firefox(self):


		#Inicia o driver do Firefox
		driver = webdriver.Firefox()

		#Timeout de espera antes do estouro de excecao
		driver.implicitly_wait(5)

		# Maximize tela do browser
		driver.maximize_window()

		driver.get(self.host)

		#Verifica se o titulo esta correto
		assert "Gerenciador Web | Intelbras" in driver.title

		# LoginTest(driver, user, pwd).invalid_login()
		LoginTest(driver, self.user, self.pwd).valid_login()

		return driver
