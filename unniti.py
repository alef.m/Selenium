import time
import sys
import os

path = os.getcwd()
sys.path.insert(0, path + "/acesso")
sys.path.insert(0, path + "/ramais")
sys.path.insert(0, path + "/configuracoes")
sys.path.insert(0, path + "/tools")


from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

from loadConfig import configs
from login import LoginTest
from ramais import RamaisTest
from driver_tools import DriverTools

# Carrega as configuracoes do arquivo config.txt
host = configs().load_config(path, 'host')
user = configs().load_config(path, 'user')
pwd = configs().load_config(path, 'pwd')
senhaRamal = configs().load_config(path, 'senhaRamal')

path = os.getcwd()
LOG_FILENAME = path + '/logs/' + str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')) + '.out'


#Inicia o driver do Firefox
driver = DriverTools(path).criar_driver_firefox()


try:

		RamaisTest(driver, senhaRamal, LOG_FILENAME).make_test(200)

finally:
	# Fecha o navegador
	driver.close()
