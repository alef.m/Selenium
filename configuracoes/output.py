from termcolor import colored

class console(object):
	def __init__(self, nome, estado):
		self.nome = nome
		self.estado = estado

	# Saida do teste no console
	def outTest(self):
		if self.estado:
			print colored('{0:.<120}{1:.>5}'.format(self.nome, "OK"), 'green')
		else:
			print colored('{0:.<120}{1:.>5}'.format(self.nome, "NOK"), 'red')
