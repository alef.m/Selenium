import os

class configs(object):

	def load_config(self, path, name):
		
		# Titulo do arquivo que sera lido
		text_file = open(path + "/configuracoes/configs.txt", "r")

		# Le todas as linhas do arquivo
		linhas = text_file.readlines()

		# For para interacao com cada linha lida
		for num in range(len(linhas)):	

			# Config = recebe linha
			config = linhas[num]

			# Split no config por =
			config = config.split('=')

			# Comparacao se o nome da config lida e o mesmo passado por parametro
			if name in config[0]:
				aux = config[1]

				# strip para retirar o espaco em branco da configuracao
				return aux.strip()


